//
//  UITextField+XibConfiguration.m
//  RestaurantApp
//
//  Created by TM iMac on 11/29/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import "UITextField+XibConfiguration.h"

@implementation UITextField (XibConfiguration)
@dynamic leftPadding, rightPadding;

- (void)setLeftPadding:(CGFloat)leftPadding{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, leftPadding, CGRectGetHeight(self.bounds))];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;}

- (void)setRightPadding:(CGFloat)rightPadding{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rightPadding, CGRectGetHeight(self.bounds))];
    self.rightView = paddingView;
    self.rightViewMode = UITextFieldViewModeAlways;
}

@end
