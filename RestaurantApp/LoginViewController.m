//
//  LoginViewController.m
//  RestaurantApp
//
//  Created by TM iMac on 1/9/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "LoginViewController.h"
#import "APIManager.h"

@interface LoginViewController () {
    
    __weak IBOutlet UITextField *passwordField;
    __weak IBOutlet UITextField *emailField;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = RestaurantName;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"close"] style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    
    emailField.text = @"shahnewaz@live.com";//@"rifat_nsu@yahoo.com";
    passwordField.text = @"123456";

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)loginBtnAct:(id)sender {

    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:emailField.text forKey:@"username"];
    [param setValue:passwordField.text forKey:@"password"];
    [[APIManager sharedManager] executePOSTRequestWith:[NSString stringWithFormat:@"CustomerLogin.php?restaurant_id=%@",RestaurantId] Parameters:param ForSuccess:^(id response) {
        NSDictionary *user = [response valueForKey:@"Message"];
        [[User sharedUser] setShared:[User objectFromDictionary:user]];
        NSLog(@"[User sharedUser]: %@",[User sharedUser].deviceToken);
        [USERDEFAULTS setValue:user forKey:kUSERSHAREDKEY];
        [USERDEFAULTS setBool:YES forKey:kLOGINKEY];
        [USERDEFAULTS synchronize];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginLogout object:nil];
        
        [self dismissViewControllerAnimated:YES completion:^{
            [TSMessage showNotificationWithTitle:@"You have been successfully logged in" type:TSMessageNotificationTypeSuccess];
        }];
    } ForFail:^(NSString *error) {
        [TSMessage showNotificationInViewController:self title:error subtitle:nil type:TSMessageNotificationTypeError];
    }];

}

- (IBAction)forgotBtnAct:(id)sender {
    
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
