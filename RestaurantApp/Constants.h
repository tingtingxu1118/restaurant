//
//  Constants.h
//  RestaurantApp
//
//  Created by TM iMac on 11/29/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#import "AppDelegate.h"
#import "DataModels.h"
#import <TSMessages/TSMessage.h>
#import "User.h"
#import "Environment.h"

#endif /* Constants_h */

#define SYSTEM_VERSION ([[[UIDevice currentDevice] systemVersion] floatValue])
#define appDelegate ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define USERDEFAULTS ([NSUserDefaults standardUserDefaults])

#define RestaurantId @"2"
#define RestaurantName @"EAT AND EAT"
#define UserToken @"12345"
#define kLOGINKEY @"isLoggedIn"
#define kUSERSHAREDKEY @"userSharedKey"

#define LATITUDE ([[NSUserDefaults standardUserDefaults] valueForKey:@"LATITUDE"])
#define LONGITUDE ([[NSUserDefaults standardUserDefaults] valueForKey:@"LONGITUDE"])

#define kNotificationLoginLogout @"loginCompleteNotification"

#define APPSUPPORTDIRECTORY ([NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) lastObject])
#define DOCUMENTDIRECTORY ([NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0])
#define BUSKETPLISTPATH ([APPSUPPORTDIRECTORY stringByAppendingPathComponent:@"busket.plist"])

#define DEGREES_TO_RADIANS(degrees) ((M_PI * degrees) / 180.0
/** Float: Radians -> Degrees **/
#define RADIANS_TO_DEGREES(radians) ((radians * 180.0)/ M_PI)
/**Navigation - Go back - POP view controller **/
#define GoBackPopViewControllerAnimated(animated) [self.navigationController popViewControllerAnimated:animated]
// Timer Invalidation
#define UA_INVALIDATE_TIMER(t) [t invalidate]; t = nil;

#define BASEAPPEARANCECOLOUR [UIColor colorWithRed:0.92 green:0.13 blue:0.18 alpha:1.00]
#define SECONDARYAPPEARANCECOLOUR [UIColor colorWithRed:0.33 green:0.78 blue:0.38 alpha:1.00]
