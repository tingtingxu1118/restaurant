//
//  DishHeaderCell.h
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 3/30/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "ChoiceCellView.h"

@protocol DishHeaderDelegate;
@interface DishHeaderCell : UITableViewCell {
    __weak IBOutlet UILabel *descriptionLbl;
    __weak IBOutlet UILabel *nameLbl;
    __weak IBOutlet UIButton *addBtn;
    __weak IBOutlet UILabel *priceLbl;
    __weak IBOutlet UIButton *favouriteBtn;

}

@property (nonatomic, retain) Dish *dish;
@property (nonatomic, weak) id<DishHeaderDelegate> delegate;

@end



@protocol DishHeaderDelegate <NSObject>
@optional
- (void)goForAddItem:(id)sender;

@end
