//
//  LeftMenuViewController.m
//  RestaurantApp
//
//  Created by TM iMac on 1/9/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "Constants.h"

@interface LeftMenuViewController ()<UITableViewDelegate, UITableViewDataSource> {
    NSArray *menuArray;
    __weak IBOutlet UIButton *loginBtn;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation LeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [self.tableView registerNib:[UINib nibWithNibName:[SideMenuHeader description] bundle:nil] forHeaderFooterViewReuseIdentifier:[SideMenuHeader description]];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.estimatedRowHeight = self.tableView.rowHeight;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"SideMenus" ofType:@"plist"];
    menuArray = [NSArray arrayWithContentsOfFile:plistPath];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessfull:) name:kNotificationLoginLogout object:nil];
    if ([USERDEFAULTS boolForKey:kLOGINKEY]) {
        [loginBtn setTitle:[NSString stringWithFormat:@"%@ %@", [User sharedUser].firstName, [User sharedUser]. lastName] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessfull:) name:kNotificationLoginComplete object:nil];
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationLoginComplete object:nil];
//}

- (void)loginSuccessfull:(NSNotification*)notification {
    if ([USERDEFAULTS boolForKey:kLOGINKEY]) {
        [loginBtn setTitle:[NSString stringWithFormat:@"%@ %@", [User sharedUser].firstName, [User sharedUser]. lastName] forState:UIControlStateNormal];
        NSLog(@"Received Notification - Someone seems to have logged in");
    }
    else {
        [loginBtn setTitle:@"Login / Sign Up" forState:UIControlStateNormal];
        NSLog(@"User have logged out");
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginBtnAct:(id)sender {
    [appDelegate.container toggleLeftSideMenuCompletion:^{
        if ([USERDEFAULTS boolForKey:kLOGINKEY]) {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
            [appDelegate.container setCenterViewController:[sb instantiateInitialViewController]];
        }
        else {
            [appDelegate goForLogin];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}
/*
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SideMenuHeader *sectionHeader = (SideMenuHeader*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:[SideMenuHeader description]];
    sectionHeader.backgroundColor = [UIColor colorWithRed:0.24 green:0.24 blue:0.24 alpha:1.00];
//    Dish *dish = _dishs[section];
//    sectionHeader.dish = dish;
    return sectionHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    SideMenuHeader *sectionHeader = (SideMenuHeader*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:[SideMenuHeader description]];
//    Dish *dish = _dishs[section];
//    sectionHeader.dish = dish;
    return [sectionHeader layoutHeightForTableView:tableView];
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menuArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SideMenuCell" forIndexPath:indexPath];
    
    // Configure the cell...
    NSDictionary *dic = [menuArray objectAtIndex:indexPath.row];
    UILabel *lbl = (UILabel*)[cell.contentView viewWithTag:100];
    lbl.text = [dic valueForKey:@"title"];
    UILabel *lbl2 = (UILabel*)[cell.contentView viewWithTag:101];
    lbl2.text = [dic valueForKey:@"subtitle"];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [appDelegate.container toggleLeftSideMenuCompletion:^{
        switch (indexPath.row) {
            case 0: {
                if ([USERDEFAULTS boolForKey:kLOGINKEY]) {
                    appDelegate.container.centerViewController = [TabBarController sharedController];
                    [[TabBarController sharedController] setSelectedIndex:0];
                }
                else {
                    [appDelegate goForLogin];
                }
            }
                break;
            case 1:
            {
                if ([USERDEFAULTS boolForKey:kLOGINKEY]) {
                    appDelegate.container.centerViewController = [TabBarController sharedController];
                    [[TabBarController sharedController] setSelectedIndex:2];

                }
                else {
                    [appDelegate goForLogin];
                }
            }
                break;
            case 2:
            {
                if ([USERDEFAULTS boolForKey:kLOGINKEY]) {
                    appDelegate.container.centerViewController = [TabBarController sharedController];
                    [[TabBarController sharedController] setSelectedIndex:0];
                }
                else {
                    [appDelegate goForLogin];
                }
            }
                break;
            case 3:
            {
                if ([USERDEFAULTS boolForKey:kLOGINKEY]) {
                    appDelegate.container.centerViewController = [TabBarController sharedController];
                    [[TabBarController sharedController] setSelectedIndex:3];
//                    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Address" bundle:nil];
//                    [appDelegate.container setCenterViewController:[sb instantiateInitialViewController]];
                }
                else {
                    [appDelegate goForLogin];
                }
            }
                break;
            case 4:
            {
                if ([USERDEFAULTS boolForKey:kLOGINKEY]) {
                    appDelegate.container.centerViewController = [TabBarController sharedController];
                    [[TabBarController sharedController] setSelectedIndex:0];
                    
                }
                else {
                    [appDelegate goForLogin];
                }
            }
                break;
            case 5:
            {
                if ([USERDEFAULTS boolForKey:kLOGINKEY]) {
                    appDelegate.container.centerViewController = [TabBarController sharedController];
                    [[TabBarController sharedController] setSelectedIndex:0];
                }
                else {
                    [appDelegate goForLogin];
                }
            }
                break;
            default:
                break;
        }
    }];
}


@end
