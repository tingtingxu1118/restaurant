//
//  ProfileViewController.m
//  RestaurantApp
//
//  Created by TM iMac on 1/10/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "ProfileViewController.h"
#import "APIManager.h"

@interface ProfileViewController () {

    __weak IBOutlet UIButton *logoutBtn;
    __weak IBOutlet UIButton *changePassBtn;
    __weak IBOutlet UITextField *emailField;
    __weak IBOutlet UITextField *phoneNumberField;
    __weak IBOutlet UITextField *lastNameField;
    __weak IBOutlet UITextField *firstNameField;
    __weak IBOutlet UITextField *dobField;
    __weak IBOutlet UITextField *doaField;
    __weak IBOutlet UITextField *mobField;
    __weak IBOutlet UITextField *moaField;

}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];

    if (_isEditable) {
        self.navigationItem.title = @"Edit Profile";
        [changePassBtn setTitle:@"Update" forState:UIControlStateNormal];
    }
    else {
        self.navigationItem.title = @"Profile";
        [changePassBtn setTitle:@"Change Password" forState:UIControlStateNormal];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"side_menu"] style:UIBarButtonItemStylePlain target:appDelegate action:@selector(toggleSideMenu:)];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"edit_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(goForEdit)];
    }
    logoutBtn.hidden = emailField.enabled = phoneNumberField.enabled = lastNameField.enabled = firstNameField.enabled = dobField.enabled = doaField.enabled = mobField.enabled = moaField.enabled = _isEditable;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setDefaults];
}

- (void)setDefaults {
    User *user = [User sharedUser];
    firstNameField.text = user.firstName;
    lastNameField.text = user.lastName;
    emailField.text = user.email;
    phoneNumberField.text = user.mobileNumber;
    dobField.text = user.dateOfBirth;
    doaField.text = user.anniversaryDate;
    mobField.text = user.monthOfBirth;
    moaField.text = user.anniversaryMonth;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)goForEdit {
    ProfileViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    vc.isEditable = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)logoutAct:(id)sender {
    [[APIManager sharedManager] executePUTRequestWith:[NSString stringWithFormat:@"CustomerLogout.php?restaurant_id=%@", RestaurantId] Parameters:@{@"customer_id": @([User sharedUser].customerId)} ForSuccess:^(id response) {
        [USERDEFAULTS setBool:NO forKey:kLOGINKEY];
        [USERDEFAULTS synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginLogout object:nil];
        [TSMessage showNotificationWithTitle:[response valueForKey:@"Message"] type:TSMessageNotificationTypeSuccess];
        appDelegate.container.centerViewController = [TabBarController sharedController];
        [[TabBarController sharedController] setSelectedIndex:0];
    } ForFail:^(NSString *error) {
        [TSMessage showNotificationWithTitle:error type:TSMessageNotificationTypeError];
    }];
    
}

- (IBAction)changePassAct:(id)sender {
    if (_isEditable) {
        User *user = [[User alloc] init];
        user.firstName = firstNameField.text;
        user.lastName = lastNameField.text;
        user.email = emailField.text;
        user.mobileNumber = phoneNumberField.text;
        user.dateOfBirth = dobField.text;
        user.monthOfBirth = mobField.text;
        user.anniversaryDate = doaField.text;
        user.anniversaryMonth = moaField.text;
//        NSDictionary *param = [[ObjectMapper sharedInstance] dictionaryFromObject:user];
//        NSDictionary *param = [NSMutableDictionary dictionary];
//        [param setValue:firstNameField.text forKey:@"first_name"];
//        [param setValue:lastNameField.text forKey:@"last_name"];
//        [param setValue:emailField.text forKey:@"email"];
//        [param setValue:phoneNumberField.text forKey:@"mobile_number"];
//        [param setValue:dobField.text forKey:@"date_of_birth"];
//        [param setValue:mobField.text forKey:@"month_of_birth"];
//        [param setValue:doaField.text forKey:@"anniversary_date"];
//        [param setValue:moaField.text forKey:@"anniversary_month"];
//        NSLog(@"Param===%@",param);
        [[APIManager sharedManager] executePUTRequestWith:[NSString stringWithFormat:@"EditCustomerProfile.php?restaurant_id=%@&customer_id=%ld", RestaurantId, (long)[User sharedUser].customerId] Parameters:[user dictionary] ForSuccess:^(id response) {
            [User sharedUser].firstName = user.firstName;
            [User sharedUser].lastName = user.lastName;
            [User sharedUser].email = user.email;
            [User sharedUser].mobileNumber = user.mobileNumber;
            [User sharedUser].dateOfBirth = user.dateOfBirth;
            [User sharedUser].monthOfBirth = user.monthOfBirth;
            [User sharedUser].anniversaryDate = user.anniversaryDate;
            [User sharedUser].anniversaryMonth = user.anniversaryMonth;
            [USERDEFAULTS setValue:[[User sharedUser] dictionary] forKey:kUSERSHAREDKEY];
            [USERDEFAULTS synchronize];
            [TSMessage showNotificationWithTitle:[response valueForKey:@"Message"] type:TSMessageNotificationTypeSuccess];
            GoBackPopViewControllerAnimated(YES);
        } ForFail:^(NSString *error) {
            [TSMessage showNotificationWithTitle:error type:TSMessageNotificationTypeError];
        }];
    }
    else {
        [self performSegueWithIdentifier:@"ChangePasswordViewController" sender:nil];
    }
}
@end
