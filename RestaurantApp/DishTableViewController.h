//
//  DishTableViewController.h
//  RestaurantApp
//
//  Created by TM iMac on 12/18/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface DishTableViewController : UITableViewController


@property (nonatomic, retain) NSArray *dishs;

@end
