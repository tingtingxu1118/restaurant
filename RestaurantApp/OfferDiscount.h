//
//  OfferDiscount.h
//  RestaurantApp
//
//  Created by The Messenger on 2/22/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfferDiscount : NSObject

@property (nonatomic) double discountAmount;
@property (nonatomic) NSString *discountTitle, *discountType, *offerTitle;
@property (nonatomic, assign) BOOL isSelected;
@end
