//
//  ServiceView.h
//  RestaurantApp
//
//  Created by The Messenger on 2/14/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceView : UIView {
    
}

@property (weak, nonatomic) IBOutlet UILabel *serviceLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;

@end
