//
//  SubmitOrderRequest.m
//  RestaurantApp
//
//  Created by The Messenger on 2/6/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "SubmitOrderRequest.h"
#import "Constants.h"

static SubmitOrderRequest* shared = nil;


@implementation SubmitOrderRequest

//+ (SubmitOrderRequest*)sharedInstance {
//    if (!shared) {
//        NSDictionary *data = [NSDictionary dictionaryWithContentsOfFile:BUSKETPLISTPATH];
////        NSLog(@"%@",data);
//        shared = [SubmitOrderRequest objectFromDictionary:data];
//    }
//    return shared;
//}

+ (SubmitOrderRequest*)instance {
    NSDictionary *data = [NSDictionary dictionaryWithContentsOfFile:BUSKETPLISTPATH];
    NSLog(@"%@",data);
    SubmitOrderRequest *request = [SubmitOrderRequest objectFromDictionary:data];
    return request;
}
@end
