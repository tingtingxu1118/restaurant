//
//  AddressCell.h
//  RestaurantApp
//
//  Created by TM iMac on 1/16/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface AddressCell : UITableViewCell {
    
    __weak IBOutlet UILabel *countryLbl;
    __weak IBOutlet UILabel *cityLbl;
    __weak IBOutlet UILabel *lineLbl;
    __weak IBOutlet UILabel *titleLbl;
}


@property (nonatomic, retain) Address *address;

@end
