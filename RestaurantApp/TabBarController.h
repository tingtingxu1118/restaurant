//
//  TabBarController.h
//  RestaurantApp
//
//  Created by TM iMac on 1/10/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarController : UITabBarController


+ (TabBarController*)sharedController;

@end
