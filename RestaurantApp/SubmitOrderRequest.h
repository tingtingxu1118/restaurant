//
//  SubmitOrderRequest.h
//  RestaurantApp
//
//  Created by The Messenger on 2/6/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface SubmitOrderRequest : NSObject

@property (nonatomic, retain) NSString *orderFor, *finalComment, *paymentMethod, *paymentStatus, *offerTitle, *discountTitle;
@property (nonatomic, retain) NSString *deliveryCollectionDateTime;
@property (nonatomic, retain) NSMutableDictionary *customerDetails;
@property (nonatomic, retain) Address *deliveryCollectionAddress;
@property (nonatomic, assign) double deliveryFee, discountAmount;
@property (nonatomic, retain) NSMutableArray *dishs;


//+ (SubmitOrderRequest*)sharedInstance;
+ (SubmitOrderRequest*)instance;

@end
