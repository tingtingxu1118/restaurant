//
//  SavedAdressViewController.h
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 1/10/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIManager.h"

typedef void(^AddressBlock)(Address *address);

@interface SavedAdressViewController : UIViewController

@property (copy) AddressBlock didSelectAddress;
//@property (copy) AddressBlock didUpdateAddress;

@end
