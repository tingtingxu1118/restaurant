//
//  CategoryTableViewCell.m
//  RestaurantApp
//
//  Created by TM iMac on 12/18/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import "CategoryTableViewCell.h"

@implementation CategoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCategory:(DishCategory *)category {
    _category = category;
    nameLbl.text = category.categoryName;
}

@end
