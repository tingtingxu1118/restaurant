//
//  SelectionHeader.h
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 2/2/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectionHeader : UITableViewHeaderFooterView

@property (weak, nonatomic) IBOutlet UILabel *nameLbl;

@end
