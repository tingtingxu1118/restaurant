//
//  CategoryTableViewCell.h
//  RestaurantApp
//
//  Created by TM iMac on 12/18/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataModels.h"

@interface CategoryTableViewCell : UITableViewCell {
    
    __weak IBOutlet UILabel *nameLbl;
}

@property (nonatomic, retain) DishCategory *category;

@end
