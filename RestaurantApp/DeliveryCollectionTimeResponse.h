//
//  DeliveryCollectionTimeResponse.h
//  RestaurantApp
//
//  Created by The Messenger on 2/20/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeliveryCollectionTimeResponse : NSObject


@property (nonatomic, copy) NSArray *deliveryCollectionTimes;

@end

@interface DeliveryCollectionTime : NSObject

@property (nonatomic, copy) NSString *dateTime;

@end
