//
//  main.m
//  RestaurantApp
//
//  Created by TM iMac on 11/2/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
