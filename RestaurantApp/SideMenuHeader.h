//
//  SideMenuHeader.h
//  RestaurantApp
//
//  Created by TM iMac on 1/9/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuHeader : UITableViewHeaderFooterView


- (CGFloat)layoutHeightForTableView:(UITableView *)tableView;

@end
