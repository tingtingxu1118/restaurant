//
//  SavedAdressViewController.m
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 1/10/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "SavedAdressViewController.h"
#import "Constants.h"
#import "AddAddressViewController.h"
#import "AddressCell.h"
#import "SubmitOrderRequest.h"

@interface SavedAdressViewController () {
    NSMutableArray *addressArray;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SavedAdressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Saved Addresses";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    if (!self.didSelectAddress) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"side_menu"] style:UIBarButtonItemStylePlain target:appDelegate action:@selector(toggleSideMenu:)];
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addAction)];

    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.estimatedRowHeight = self.tableView.rowHeight;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    [self getAddresses];
//    [TSMessage showNotificationWithTitle:@"Swipe left with two fingers for edit or delete" type:TSMessageNotificationTypeMessage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:[AddAddressViewController description]]) {
        AddAddressViewController *vc = segue.destinationViewController;
        if (sender != nil) {
            vc.address = sender;
        }
        __weak typeof(self) weakSelf = self;
        vc.didAddNewAddress = ^(Address *address) {
            [addressArray insertObject:address atIndex:0];
            [weakSelf.tableView reloadData];
        };
        vc.didUpdateAddress = ^(Address *address) {
            NSInteger index = 0;
            for (Address *adrs in addressArray) {
                if (address.addressId == adrs.addressId) {
                    [addressArray replaceObjectAtIndex:index withObject:address];
                    [weakSelf.tableView reloadData];
                    break;
                }
                index++;
            }
        };
    }
}


- (void)getAddresses {
    [[APIManager sharedManager] executeGETRequestWith:[NSString stringWithFormat:@"GetAddress.php?restaurant_id=%@&customer_id=%ld",RestaurantId, (long)[User sharedUser].customerId] Parameters:nil ForSuccess:^(id response) {
        addressArray = [NSMutableArray arrayWithArray:[Address objectFromDictionary:[response valueForKey:@"Message"]]];
        [_tableView reloadData];
    } ForFail:^(NSString *error) {
        [TSMessage showNotificationWithTitle:error type:TSMessageNotificationTypeError];
    }];
}

- (void)addAction {
    [self performSegueWithIdentifier:[AddAddressViewController description] sender:nil];
}

#pragma mark - Table view data source
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {

    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [tableView setEditing:NO];
        [self performSegueWithIdentifier:[AddAddressViewController description] sender:addressArray[indexPath.row]];
    }];
    editAction.backgroundColor = [UIColor blueColor];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [tableView setEditing:NO];
        Address *address = addressArray[indexPath.row];
        [[APIManager sharedManager] executeDELETERequestWith:[NSString stringWithFormat:@"DeleteAddress.php?restaurant_id=%@&customer_id=%ld&&address_id=%ld",RestaurantId, (long)[User sharedUser].customerId, (long)address.addressId] Parameters:nil ForSuccess:^(id response) {
            [TSMessage showNotificationWithTitle:[response valueForKey:@"Message"] type:TSMessageNotificationTypeSuccess];
            [addressArray removeObject:address];
            [tableView reloadData];
        } ForFail:^(NSString *error) {
            [TSMessage showNotificationWithTitle:error type:TSMessageNotificationTypeError];
        }];
    }];
    deleteAction.backgroundColor = [UIColor redColor];
    return @[deleteAction,editAction];
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
////        [_chats removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//    }
//    else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        
//    }
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return addressArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:[AddressCell description] forIndexPath:indexPath];
    // Configure the cell...
    cell.address = addressArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (self.didSelectAddress) {
        SubmitOrderRequest *submitOrderRequ = [SubmitOrderRequest instance];
        submitOrderRequ.deliveryCollectionAddress = addressArray[indexPath.row];
        [[submitOrderRequ dictionary] writeToFile:BUSKETPLISTPATH atomically: YES];
//        self.didSelectAddress(addressArray[indexPath.row]);
        GoBackPopViewControllerAnimated(YES);
    }
}

@end
