//
//  SideMenuHeader.m
//  RestaurantApp
//
//  Created by TM iMac on 1/9/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "SideMenuHeader.h"

@implementation SideMenuHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (CGFloat)layoutHeightForTableView:(UITableView *)tableView {
    CGFloat height = [self systemLayoutSizeFittingSize:CGSizeMake(CGRectGetWidth(tableView.bounds), 0) withHorizontalFittingPriority:UILayoutPriorityRequired verticalFittingPriority:UILayoutPriorityDefaultLow].height;
    
    return height + 1;
}

@end
