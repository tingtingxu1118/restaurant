//
//  ChoiceTableViewCell.m
//  RestaurantApp
//
//  Created by TM iMac on 12/18/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import "ChoiceTableViewCell.h"

@implementation ChoiceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDishCombination:(DishCombination *)dishCombination {
    _dishCombination = dishCombination;
    priceLbl.text = [NSString stringWithFormat:@"£%.2f", _dishCombination.combPrice];
    titleLbl.text = _dishCombination.combName;
//    descriptionLbl.text = dish.dishDescription;
    
}

- (IBAction)addButtonAct:(id)sender {
    if ([self.delegate respondsToSelector:@selector(goForAddItem:)]) {
        [self.delegate goForAddItem:self];
    }
}

@end
