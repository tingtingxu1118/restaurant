//
//  DishSectionHeader.m
//  RestaurantApp
//
//  Created by TM iMac on 12/18/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import "DishSectionHeader.h"

@implementation DishSectionHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib {
    [super awakeFromNib];
//    addBtn.backgroundColor = BASEAPPEARANCECOLOUR;
    UIView *bg = [self viewWithTag:100];
    bg.backgroundColor = [UIColor whiteColor];
}

- (void)setDish:(Dish *)dish {
    _dish = dish;
    addBtn.hidden = priceLbl.hidden = (dish.dishCombinations.count > 0);
    priceLbl.text = [NSString stringWithFormat:@"£%.2f",dish.dishPrice];
    nameLbl.text = dish.dishName;
    descriptionLbl.text = dish.dishDescription;
    
}

- (CGFloat)layoutHeightForTableView:(UITableView *)tableView {
    CGFloat height = [self systemLayoutSizeFittingSize:CGSizeMake(CGRectGetWidth(tableView.bounds), 0) withHorizontalFittingPriority:UILayoutPriorityRequired verticalFittingPriority:UILayoutPriorityDefaultLow].height;
    
    return height + 1;
}

- (IBAction)addButtonAct:(id)sender {
    if ([self.delegate respondsToSelector:@selector(goForAddItem:)]) {
        [self.delegate goForAddItem:self.dish];
    }
}

@end
