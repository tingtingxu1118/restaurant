//
//  ChoiceCellView.m
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 3/30/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "ChoiceCellView.h"

@implementation ChoiceCellView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (instancetype)viewWithCombination:(DishCombination *)combination {
    ChoiceCellView *rootView = [[[NSBundle mainBundle] loadNibNamed:[ChoiceCellView description] owner:self options:nil] objectAtIndex:0];
    rootView.dishCombination = combination;
    return rootView;
}


- (void)setDishCombination:(DishCombination *)dishCombination {
    _dishCombination = dishCombination;
    priceLbl.text = [NSString stringWithFormat:@"£%.2f", _dishCombination.combPrice];
    titleLbl.text = _dishCombination.combName;
    //    descriptionLbl.text = dish.dishDescription;
}

- (IBAction)addButtonAct:(id)sender {
    if ([self.delegate respondsToSelector:@selector(goForAddItem:)]) {
        [self.delegate goForAddItem:self];
    }
}

@end
