//
//  ObjectMapingConfiguration.h
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 2/20/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <OCMapper/OCMapper.h>

@interface ObjectMapingConfiguration : NSObject

+ (InCodeMappingProvider*)getInCodeMappingProvider;

@end
