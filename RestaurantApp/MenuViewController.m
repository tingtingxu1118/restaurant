//
//  MenuViewController.m
//  RestaurantApp
//
//  Created by TM iMac on 11/29/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import "MenuViewController.h"
#import "APIManager.h"
#import "CategoryTableViewCell.h"
#import "AddItemViewController.h"
#import "DishHeaderCell.h"

@interface MenuViewController () <UITableViewDelegate, UITableViewDataSource, DishHeaderDelegate> {
    RestaurantData *_restaurantData;
    __weak IBOutlet UITableView *_tableView;
    NSMutableIndexSet *expandedSections;
    __weak IBOutlet UITextField *searchField;
    NSArray *searchList;
}

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = RestaurantName;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"side_menu"] style:UIBarButtonItemStylePlain target:appDelegate action:@selector(toggleSideMenu:)];
    if (!expandedSections) {
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tableView.estimatedRowHeight = _tableView.rowHeight;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    [_tableView registerNib:[UINib nibWithNibName:[DishHeaderCell description] bundle:nil] forCellReuseIdentifier:[DishHeaderCell description]];
//    [self loadRestaurantMenu];
    _restaurantData = [RestaurantData sharedData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
//    if ([segue.identifier isEqualToString:[DishTableViewController description]]) {
//        DishTableViewController *vc = segue.destinationViewController;
//        vc.dishs = (NSArray*)sender;
//    }
    if ([segue.identifier isEqualToString:[AddItemViewController description]]) {
        AddItemViewController *vc = segue.destinationViewController;
        if ([sender isKindOfClass:[Dish class]]) {
            NSDictionary *dish = [(Dish*)sender dictionary];
            vc.dish = [Dish objectFromDictionary:dish];
        }
        else if ([sender isKindOfClass:[ChoiceCellView class]]) {
            ChoiceCellView *cell = (ChoiceCellView*)sender;
            vc.dishCombination = [DishCombination objectFromDictionary:[cell.dishCombination dictionary]];
            vc.dish = [Dish objectFromDictionary:[cell.dish dictionary]];
        }
    }
}

- (void)goForAddItem:(id)sender {
    [self performSegueWithIdentifier:[AddItemViewController description] sender:sender];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section {
    return searchField.text.length == 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (searchField.text.length > 0) {
        return 1;
    }
    return _restaurantData.dishCategorys.count;
}

//- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    DishCategory *cat = _restaurantData.dishCategorys[section];
//    return cat.categoryName;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (searchField.text.length > 0) {
        return searchList.count;
    }
    if ([self tableView:tableView canCollapseSection:section]) {
        if ([expandedSections containsIndex:section]) {
            DishCategory *cat = _restaurantData.dishCategorys[section];
            return cat.dishs.count+1;
        }
        return 1;
    }
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (searchField.text.length > 0) {
        DishHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:[DishHeaderCell description] forIndexPath:indexPath];
        cell.dish = searchList[indexPath.row];
        cell.delegate = self;
        return cell;
    } else if ([self tableView:tableView canCollapseSection:indexPath.section]) {
        if (!indexPath.row) {
            // first row
            CategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[CategoryTableViewCell description] forIndexPath:indexPath];
            DishCategory *cat = _restaurantData.dishCategorys[indexPath.section];
            cell.category = cat;
            UIImageView *iv = [cell.contentView viewWithTag:123];
            if ([expandedSections containsIndex:indexPath.section]) {
                iv.image = [UIImage imageNamed:@"arrow_up"];
            }
            else {
                iv.image = [UIImage imageNamed:@"arrow_down"];
            }
            return cell;
        }
        else {
            // all other rows
            DishHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:[DishHeaderCell description] forIndexPath:indexPath];
            DishCategory *cat = _restaurantData.dishCategorys[indexPath.section];
            cell.dish = cat.dishs[indexPath.row-1];
            cell.delegate = self;
            return cell;
        }
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (searchField.text.length < 1 && [self tableView:tableView canCollapseSection:indexPath.section]) {
        if (!indexPath.row) {
            // only first row toggles exapand/collapse
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            NSInteger section = indexPath.section;
            BOOL currentlyExpanded = [expandedSections containsIndex:section];
            NSInteger rows;
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            if (currentlyExpanded)
            {
                rows = [self tableView:tableView numberOfRowsInSection:section];
                [expandedSections removeIndex:section];
                
            }
            else
            {
                [expandedSections addIndex:section];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }
            
            for (int i=1; i<rows; i++)
            {
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                               inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }
            
            CategoryTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            UIImageView *iv = [cell.contentView viewWithTag:123];
            if (currentlyExpanded) {
                [tableView deleteRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
                iv.image = [UIImage imageNamed:@"arrow_down"];
            }
            else {
                [tableView insertRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
                iv.image = [UIImage imageNamed:@"arrow_up"];
            }
        }
        else {
            NSLog(@"Selected Section is %ld and subrow is %ld ",(long)indexPath.section ,(long)indexPath.row);
        }
        
    }
    else {

    }
    
}
//{
//    [tableView deselectRowAtIndexPath:indexPath animated:NO];
////    CategoryTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
////    [self performSegueWithIdentifier:[DishTableViewController description] sender:cell.category.dishs];
//}

- (IBAction)editingChanged:(UITextField*)sender {
//    NSLog(@"%@", sender.text);
    NSPredicate *predicateName = [NSPredicate predicateWithFormat:@"dishName contains[cd] %@", sender.text];
    NSPredicate *predicateDescription = [NSPredicate predicateWithFormat:@"dishDescription contains[cd] %@", sender.text];
    NSArray *subPredicates = [NSArray arrayWithObjects:predicateName, predicateDescription, nil];
    searchList = [_restaurantData.dishs filteredArrayUsingPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:subPredicates]];
    [_tableView reloadData];
}

@end
