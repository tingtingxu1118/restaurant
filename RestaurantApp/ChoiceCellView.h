//
//  ChoiceCellView.h
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 3/30/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@protocol ChoiceCellDelegate;
@interface ChoiceCellView : UIView {
    __weak IBOutlet UILabel *titleLbl;
    __weak IBOutlet UILabel *priceLbl;
}

+ (instancetype) viewWithCombination:(DishCombination*)combination;

@property (nonatomic, retain) DishCombination *dishCombination;
@property (nonatomic, retain) Dish *dish;
@property (nonatomic, weak) id<ChoiceCellDelegate> delegate;

@end


@protocol ChoiceCellDelegate <NSObject>
@optional
- (void)goForAddItem:(ChoiceCellView*)sender;

@end
