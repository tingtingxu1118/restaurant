//
//  UITextView+XibConfiguration.m
//  HeadsUp7
//
//  Created by TM iMac on 1/30/17.
//  Copyright © 2017 tm. All rights reserved.
//

#import "UITextView+XibConfiguration.h"

@implementation UITextView (XibConfiguration)
@dynamic placeholder;

- (void)setPlaceholder:(NSString *)placeholder {
    UILabel *placeholderLabel = [self viewWithTag:5767];
    if (placeholderLabel==nil) {
        placeholderLabel = [[UILabel alloc] init];
        placeholderLabel.tag = 5767;
        placeholderLabel.numberOfLines = 0;
        placeholderLabel.textColor = [UIColor colorWithWhite:0 alpha:0.3];
        [self addSubview:placeholderLabel];
    }
    placeholderLabel.text = placeholder;
    placeholderLabel.font = self.font;
    [placeholderLabel sizeToFit];
    CGRect f = placeholderLabel.frame;
    f.origin.x = 5;
    f.origin.y = self.font.pointSize/2;
    placeholderLabel.frame = f;
    [self updatePlaceholderVisibility];
}

- (void)updatePlaceholderVisibility {
    UILabel *placeholderLabel = [self viewWithTag:5767];
    if (placeholderLabel) {
        placeholderLabel.hidden = self.text.length>0;
    }
}
@end
