//
//  CheckoutViewController.h
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 3/18/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OfferDiscount.h"

@interface CheckoutViewController : UIViewController

@property (nonatomic, retain) OfferDiscount *selectedOffer;
@property (nonatomic, assign) double totalPrice;

@end
