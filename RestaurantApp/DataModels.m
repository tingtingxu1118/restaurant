//
//  DataModels.m
//  RestaurantApp
//
//  Created by TM iMac on 11/29/16.
// copyright © 2016 DreamAppSolution. All rights reserved.
//

#import "DataModels.h"
#import "User.h"

@implementation DishCategory

- (id)copyWithZone:(NSZone*)zone {
    DishCategory *copy = [[DishCategory  allocWithZone:zone] init];
    [copy setCategoryId:self.categoryId];
    [copy setCategoryName:[self.categoryName copy]];
    [copy setDishs:[self.dishs copy]];
    return copy;
}

@end


@implementation DailyHours

- (id)copyWithZone:(NSZone*)zone {
    DailyHours *copy = [[DailyHours  allocWithZone:zone] init];
    [copy setMonday:[self.Monday copy]];
    [copy setTuesday:[self.Tuesday copy]];
    [copy setWednesday:[self.Wednesday copy]];
    [copy setThursday:[self.Thursday copy]];
    [copy setFriday:[self.Friday copy]];
    [copy setSaturday:[self.Saturday copy]];
    [copy setSunday:[self.Sunday copy]];
    return copy;
}
//+ (NSDictionary*)mts_arrayClassMapping {
//    return @{
//             mts_key(Monday): TimeSlot.class,
//             mts_key(Tuesday): TimeSlot.class,
//             mts_key(Wednesday): TimeSlot.class,
//             mts_key(Thursday): TimeSlot.class,
//             mts_key(Friday): TimeSlot.class,
//             mts_key(Saturday): TimeSlot.class,
//             mts_key(Sunday): TimeSlot.class
//             };
//}

@end


@implementation TimeSlot
@end



@implementation DeliveryPostcode
@end


@implementation DiscountException
@end


@implementation Discount
@end


@implementation Offer
@end


@implementation RestaurantPaymentType
@end


@implementation DishKey
@end


@implementation Choice

- (id)copyWithZone:(NSZone*)zone {
    Choice *copy = [[Choice  allocWithZone:zone] init];
    [copy setId:self.id];
    [copy setChoiceName:[self.choiceName copy]];
    [copy setPrice:self.price];
    [copy setIsSelected:self.isSelected];
    return copy;
}

@end


@implementation GroupsChoice
/*
 @property (nonatomic, retain) NSString *groupName, *groupDescription;
 @property (nonatomic, assign) NSInteger groupId, groupMinSelect, groupMaxSelect, groupIncludeSelect;
 @property (nonatomic, retain) NSArray *choices;

*/

- (id)copyWithZone:(NSZone*)zone {
    GroupsChoice *copy = [[GroupsChoice  allocWithZone:zone] init];
    [copy setGroupId:self.groupId];
    [copy setGroupName:[self.groupName copy]];
    [copy setGroupDescription:[self.groupDescription copy]];
    [copy setGroupIncludeSelect:self.groupIncludeSelect];
    [copy setGroupMinSelect:self.groupMinSelect];
    [copy setGroupMaxSelect:self.groupMaxSelect];
    [copy setChoices:[self.choices copy]];
    return copy;
}

@end


@implementation DishCombination

- (id)copyWithZone:(NSZone*)zone {
    DishCombination *copy = [[DishCombination  allocWithZone:zone] init];
    [copy setCombId:self.combId];
    [copy setCombName:[self.combName copy]];
    [copy setCombPrice:self.combPrice];
    [copy setCombDescription:[self.combDescription copy]];
    [copy setGroupsChoices:[self.groupsChoices copy]];
    return copy;
}

@end


@implementation Dish

- (id)copyWithZone:(NSZone*)zone {
    Dish *copy = [[Dish allocWithZone:zone] init];
    [copy setDishName:[self.dishName copy]];
    [copy setDishId:self.dishId];
    [copy setDishKeys:[self.dishKeys copy]];
    [copy setChoices:[self.choices mutableCopy]];
    [copy setCategory:[self.category copyWithZone:zone]];
    [copy setQuantity:self.quantity];
    [copy setCuisineId:self.cuisineId];
    [copy setDishPrice:self.dishPrice];
    [copy setTotalPrice:self.totalPrice];
    [copy setSpecialRequest:[self.specialRequest copy]];
    [copy setAllergenInfos:[self.allergenInfos copy]];
    [copy setGroupsChoices:[self.groupsChoices copy]];
    [copy setDishCombinations:[self.dishCombinations copy]];
    [copy setDishDescription:[self.dishDescription copy]];
    return copy;
}

@end


static RestaurantData *Shared = nil;

@implementation RestaurantData

//- (id)copyWithZone:(NSZone *)zone {
//    RestaurantData *copy = [[RestaurantData allocWithZone:zone] init];
//    [copy setRestaurantLogo:[self.restaurantLogo copy]];
//    [copy setRestaurantId:self.restaurantId];
//    [copy setDeliveryTimeText:[self.deliveryTimeText copy]];
//    [copy setCollectionTime:[self.collectionTime copy]];
//    [copy setCollectionTimeText:[self.collectionTimeText copy]];
//    [copy setBusinessHours:[self.businessHours copyWithZone:zone]];
//    [copy setDeliveryHours:[self.deliveryHours copyWithZone:zone]];
//    [copy setStatus:self.status];
//    [copy setIsTableBooking:self.isTableBooking];
//    [copy setIsDelivery:self.isDelivery];
//    [copy setIsCollection:self.isCollection];
//    [copy setRestaurantPaymentTypes:[self.restaurantPaymentTypes copy]];
//    [copy setOffers:[self.offers copy]];
//    [copy setDiscounts:[self.discounts copy]];
//    [copy setDishFavourites:[self.dishFavourites copy]];
//    [copy setDishs:[self.dishs copy]];
//    [copy setDishCategorys:[self.dishCategorys copy]];
//    [copy setDeliveryRadius:[self.deliveryRadius copy]];
//    [copy setDeliveryPostcodes:[self.deliveryPostcodes copy]];
//
//    return copy;
//}

+ (RestaurantData*) sharedData {
    if (Shared == nil) {
        Shared = [[RestaurantData alloc] init];
    }
    return Shared;
}

+ (void)storeSharedDataWith:(NSDictionary *)info {
    Shared = [RestaurantData objectFromDictionary:info];
}

@end

