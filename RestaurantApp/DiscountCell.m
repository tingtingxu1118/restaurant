//
//  DiscountCell.m
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 2/21/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "DiscountCell.h"

@implementation DiscountCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDiscount:(OfferDiscount *)discount {
    _discount = discount;
    applyBtn.hidden = _discount.isSelected;
    if (discount.offerTitle.length > 0) {
        titleLabel.text = discount.offerTitle;
    }
    else {
        titleLabel.text = discount.discountTitle;
    }
}

- (IBAction)discountAddBtnAct:(UIButton*)sender {
    if ([self.delegate respondsToSelector:@selector(discountAddBtnAct:)]) {
        [self.delegate discountAddBtnAct:self];
    }
}
- (IBAction)discountRemoveBtnAct:(UIButton*)sender {
    if ([self.delegate respondsToSelector:@selector(discountRemoveBtnAct:)]) {
        [self.delegate discountRemoveBtnAct:self];
    }
}

@end
