//
//  OrderRequest.h
//  RestaurantApp
//
//  Created by The Messenger on 2/6/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "DataModels.h"

@interface OrderRequest : NSObject

@property (nonatomic, retain) NSString *order_for, *payment_method, *payment_status, *offer_title, *discount_title, *final_comment;
@property (nonatomic, retain) NSDate *delivery_collection_date_time;
@property (nonatomic, retain) Address *delivery_collection_address;
@property (nonatomic, assign) double delivery_fee, discount_amount;
@property (nonatomic, retain) NSMutableArray *dishs;
@property (nonatomic, retain) NSMutableDictionary *customer_details;

@end
