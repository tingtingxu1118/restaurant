//
//  CheckoutViewController.m
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 3/18/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "CheckoutViewController.h"
#import "BusketItemCell.h"
#import "SubmitOrderRequest.h"
#import "DiscountCell.h"
#import "User.h"
#import "APIManager.h"

@interface CheckoutViewController ()<UITableViewDelegate, UITableViewDataSource> {
    __weak IBOutlet UILabel *instructionsLbl;
    __weak IBOutlet NSLayoutConstraint *itemTableHeightContraint;
    __weak IBOutlet UITableView *itemTable;
    __weak IBOutlet UITextField *phoneField;
    __weak IBOutlet UITextField *nameField;
    __weak IBOutlet UILabel *timeLbl;
    __weak IBOutlet UILabel *addressLbl;
    __weak IBOutlet UILabel *orderForLbl;
    __weak IBOutlet UILabel *totalAmountLbl;

    SubmitOrderRequest *submitOrderRequ;
    NSString *payment_method;
    
    __weak IBOutlet NSLayoutConstraint *topViewHeightConstraint;
    __weak IBOutlet UIButton *confirmBtn;
    __weak IBOutlet UILabel *contactLbl;
    __weak IBOutlet UILabel *orderNoLbl;
    
}

@end

@implementation CheckoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Checkout and Payment";
    itemTable.estimatedRowHeight = itemTable.rowHeight;
    itemTable.rowHeight = UITableViewAutomaticDimension;
    payment_method = @"Cash";
    topViewHeightConstraint.constant = 10.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    submitOrderRequ = [SubmitOrderRequest instance];
//    RestaurantData *restaurantData = [RestaurantData sharedData];
//    deliveryView.timeLabel.text = restaurantData.deliveryTimeText;
//    collectionView.timeLabel.text = restaurantData.collectionTimeText;
//    [self setServiceForView:[submitOrderRequ.orderFor isEqualToString:@"Collection"]?collectionView:deliveryView];
//    [self showAddressOnLabel];
    
//    [itemTable reloadData];
    
    nameField.text = [NSString stringWithFormat:@"%@ %@", [User sharedUser].firstName, [User sharedUser].lastName];
    phoneField.text = [User sharedUser].mobileNumber;
    orderForLbl.text = submitOrderRequ.orderFor;
    [self showAddressOnLabel];
    timeLbl.text = submitOrderRequ.deliveryCollectionDateTime;
    [itemTable layoutIfNeeded];
    itemTableHeightContraint.constant = itemTable.contentSize.height;
    totalAmountLbl.text = [NSString stringWithFormat:@"£%.2f", _totalPrice];
    instructionsLbl.text = submitOrderRequ.finalComment;
    
}
- (void)showAddressOnLabel {
    NSString *line;
    if (submitOrderRequ.deliveryCollectionAddress) {
        if (submitOrderRequ.deliveryCollectionAddress.addressLine1) {
            line = submitOrderRequ.deliveryCollectionAddress.addressLine1;
        }
        //        NSString *line = submitOrderRequ.deliveryCollectionAddress.addressLine1;
        if (submitOrderRequ.deliveryCollectionAddress.addressLine2.length>0) {
            line = [NSString stringWithFormat:@"%@ %@",line, submitOrderRequ.deliveryCollectionAddress.addressLine2];
        }
        if (submitOrderRequ.deliveryCollectionAddress.townCity.length > 0) {
            line = [NSString stringWithFormat:@"%@ %@",line, submitOrderRequ.deliveryCollectionAddress.townCity];
        }
        if (submitOrderRequ.deliveryCollectionAddress.postCode.length > 0) {
            line = [NSString stringWithFormat:@"%@ %@",line, submitOrderRequ.deliveryCollectionAddress.postCode];
        }
    }
    addressLbl.text = line;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)typeBtnAct:(UIButton *)sender {
    if (topViewHeightConstraint.constant > 10) {
        return;
    }
    for (NSInteger b=100; b<=200; b+=100) {
        UIButton *btn = [self.view viewWithTag:b];
        btn.selected = NO;
    }
    sender.selected = YES;
    payment_method = (sender.tag==100)?@"Cash":@"Card";
}
- (IBAction)confirmAct:(id)sender {
    if (nameField.text.length <1) {
        [TSMessage showNotificationWithTitle:@"Please add your name." type:TSMessageNotificationTypeError];
        return;
    }
    if (phoneField.text.length <1) {
        [TSMessage showNotificationWithTitle:@"Please add your phone number." type:TSMessageNotificationTypeError];
        return;
    }

    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:[submitOrderRequ dictionary]];
//    NSLog(@"submitOrderRequ:%@", param);
    NSMutableDictionary *user = [NSMutableDictionary dictionary];
    [user setValue:nameField.text forKey:@"customer_name"];
    [user setValue:phoneField.text forKey:@"contact_number"];
    if ([USERDEFAULTS boolForKey:kLOGINKEY]) {
        [user setValue:@([User sharedUser].customerId) forKey:@"customer_id"];
        [user setValue:[User sharedUser].email forKey:@"email"];
    }
    else {
        [user setValue:@(1) forKey:@"customer_id"];
        [user setValue:@"" forKey:@"email"];
    }
    [param setValue:user forKey:@"customer_details"];
    [param setValue:payment_method forKey:@"payment_method"];
    [param setValue:@"Not Paid" forKey:@"payment_status"];
    [[APIManager sharedManager] executePOSTRequestWith:[NSString stringWithFormat:@"SubmitOrder.php?restaurant_id=%@",RestaurantId] Parameters:param ForSuccess:^(id response) {
        NSLog(@"Success: %@", response);
        [TSMessage showNotificationWithTitle:@"Order saved successfully" type:TSMessageNotificationTypeSuccess];
        self.navigationItem.title = @"Order Confirmation";
        topViewHeightConstraint.constant = 110;
        nameField.enabled = phoneField.enabled = NO;
        confirmBtn.hidden = YES;
        orderNoLbl.text = [[response valueForKey:@"order_id"] stringValue];
        contactLbl.text = [response valueForKey:@"contact_information"];

        NSString *defaultDBPath = [[NSBundle mainBundle] pathForResource:@"Busket" ofType:@"plist"];
        NSDictionary *defaultJson = [NSDictionary dictionaryWithContentsOfFile:defaultDBPath];
        [defaultJson writeToFile:BUSKETPLISTPATH atomically: YES];
    } ForFail:^(NSString *error) {
        [TSMessage showNotificationWithTitle:error type:TSMessageNotificationTypeError];
    }];
}
/*
 "contact_information" = "Please contact us @ 07540399222 for any assistance.";
 "order_id" = 73;

 "customer_id":1,
 "customer_name":"Rifat Ahmed",
 "email":"rifat_nsu@yahoo.com",
 "contact_number":"07540272726"
*/
#pragma mark - UITableViewDelegate, UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return submitOrderRequ.dishs.count;
            break;
        case 1:
            return (_selectedOffer==nil)?0:1;
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        BusketItemCell *cell = [tableView dequeueReusableCellWithIdentifier:[BusketItemCell description] forIndexPath:indexPath];
        cell.dish = submitOrderRequ.dishs[indexPath.row];
        return cell;
    }
    else if (indexPath.section == 1) {
        DiscountCell *cell = [tableView dequeueReusableCellWithIdentifier:[DiscountCell description] forIndexPath:indexPath];
//        cell.delegate = self;
        if (_selectedOffer != nil) {
            cell.discount = _selectedOffer;
            cell.amountLbl.hidden = NO;
            cell.amountLbl.text = [NSString stringWithFormat:@"£%.2f", submitOrderRequ.discountAmount];
        }

        return cell;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DeliveryCell" forIndexPath:indexPath];
    return cell;
}
@end
