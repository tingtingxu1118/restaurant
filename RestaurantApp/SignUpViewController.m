//
//  SignUpViewController.m
//  RestaurantApp
//
//  Created by TM iMac on 1/9/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "SignUpViewController.h"
#import "APIManager.h"

@interface SignUpViewController () {
    __weak IBOutlet UITextField *passwordField;
    __weak IBOutlet UITextField *emailField;
    __weak IBOutlet UITextField *phoneNumberField;
    __weak IBOutlet UITextField *lastNameField;
    __weak IBOutlet UITextField *firstNameField;
    __weak IBOutlet UITextField *dobField;
    __weak IBOutlet UITextField *doaField;
    __weak IBOutlet UITextField *mobField;
    __weak IBOutlet UITextField *moaField;

}

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Sign Up";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"close"] style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
//    [appDelegate checkLocationServicesAndStartUpdatesForController:self];
}
    

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [appDelegate startUpdatingLocation];
    [appDelegate checkLocationServicesAndStartUpdatesForController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginBtnAct:(id)sender {
    GoBackPopViewControllerAnimated(YES);
}


- (IBAction)signUpBtnAct:(id)sender {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:firstNameField.text forKey:@"first_name"];
    [param setValue:emailField.text forKey:@"email"];
    [param setValue:passwordField.text forKey:@"password"];
    [param setValue:lastNameField.text forKey:@"last_name"];
    [param setValue:phoneNumberField.text forKey:@"mobile_number"];
    [param setValue:dobField.text forKey:@"date_of_birth"];
    [param setValue:mobField.text forKey:@"month_of_birth"];
    [param setValue:doaField.text forKey:@"anniversary_date"];
    [param setValue:moaField.text forKey:@"anniversary_month"];
    [param setValue:[NSString stringWithFormat:@"iOS %.1f", SYSTEM_VERSION] forKey:@"device_type"];
    [param setValue:[User sharedUser].deviceToken forKey:@"device_token"];
    [param setValue:[User sharedUser].deviceToken forKey:@"push_token"];
    [param setValue:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"app_version"];
    [param setValue:[NSString stringWithFormat:@"%@, %@", LATITUDE, LONGITUDE] forKey:@"geo_location"];

//    NSLog(@"Param: %@",param);
    [[APIManager sharedManager] executePOSTRequestWith:[NSString stringWithFormat:@"CreateCustomerAccount.php?restaurant_id=%@",RestaurantId] Parameters:param ForSuccess:^(id response) {
        NSDictionary *user = [response valueForKey:@"Message"];
        [[User sharedUser] setShared:[User objectFromDictionary:user]];
        NSLog(@"[User sharedUser]: %@",[User sharedUser].deviceToken);
        [USERDEFAULTS setValue:user forKey:kUSERSHAREDKEY];
        [USERDEFAULTS setBool:YES forKey:kLOGINKEY];
        [USERDEFAULTS synchronize];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginLogout object:nil];
        [self dismissViewControllerAnimated:YES completion:^{
            [TSMessage showNotificationWithTitle:@"You have been successfully logged in" type:TSMessageNotificationTypeSuccess];
        }];
    } ForFail:^(NSString *error) {
        [TSMessage showNotificationInViewController:self title:error subtitle:nil type:TSMessageNotificationTypeError];
    }];
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
