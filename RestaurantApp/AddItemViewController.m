//
//  AddItemViewController.m
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 1/27/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "AddItemViewController.h"
#import "SelectionHeader.h"
#import "SubmitOrderRequest.h"

@interface AddItemViewController ()<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource> {
    __weak IBOutlet UITextField *quantityField;
    __weak IBOutlet UILabel *nameLbl;
    __weak IBOutlet UILabel *priceLbl;
    __weak IBOutlet UILabel *descriptionLbl;
    double unitPrice;
    __weak IBOutlet UILabel *cartPriceLbl;
    __weak IBOutlet UILabel *cartLbl;
    __weak IBOutlet UITextView *instructionField;
    

}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation AddItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = _dish.dishName;
    [self.tableView registerNib:[UINib nibWithNibName:[SelectionHeader description] bundle:nil] forHeaderFooterViewReuseIdentifier:[SelectionHeader description]];
    
    [self setUpHeader];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];

}

- (void)setUpHeader {
    NSString *string = @"";
    if (_dishCombination != nil) {
        nameLbl.text = _dishCombination.combName;
        string = _dishCombination.combDescription;
        priceLbl.text = [NSString stringWithFormat:@"£%.2f",_dishCombination.combPrice];
    }
    else {
        nameLbl.text = _dish.dishName;
        string = _dish.dishDescription;
        priceLbl.text = [NSString stringWithFormat:@"£%.2f",_dish.dishPrice];
    }
    NSMutableAttributedString * attrString = [[NSMutableAttributedString alloc] initWithData:[string dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    [attrString addAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13], NSForegroundColorAttributeName : descriptionLbl.textColor} range:NSMakeRange(0, attrString.length)];
    descriptionLbl.attributedText = attrString;

    [self calculateUnitPrice];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)quantityBtnsAct:(UIButton *)sender {
    NSInteger quan = [quantityField.text integerValue];
    quantityField.text = [@(quan+sender.tag) stringValue];
    [self textFieldDidEnd:quantityField];
}


- (IBAction)textFieldDidEnd:(UITextField *)sender {
    if ([sender.text doubleValue] < 1) {
        sender.text = @"1";
    }
    [self updateCartLbl];
}

- (void)calculateUnitPrice {
    unitPrice = _dish.dishPrice + _dishCombination.combPrice;
    NSArray *groupsChoices = (_dishCombination==nil)?_dish.groupsChoices:_dishCombination.groupsChoices;
    for (GroupsChoice *group in groupsChoices) {
        for (Choice *choice in group.choices) {
            if (choice.isSelected && choice.price>0) {
                unitPrice += choice.price;
            }
        }
    }
    [self updateCartLbl];
}

- (void)updateCartLbl {
    cartLbl.text = [NSString stringWithFormat:@"Add %@ in cart", quantityField.text];
    cartPriceLbl.text = [NSString stringWithFormat:@"£%.2f" ,unitPrice*[quantityField.text integerValue]];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSArray *groupsChoices = (_dishCombination==nil)?_dish.groupsChoices:_dishCombination.groupsChoices;
    return groupsChoices.count;
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SelectionHeader *sectionHeader = (SelectionHeader*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:[SelectionHeader description]];
    NSArray *groupsChoices = (_dishCombination==nil)?_dish.groupsChoices:_dishCombination.groupsChoices;
    GroupsChoice *gc = groupsChoices[section];
    sectionHeader.nameLbl.text = gc.groupName;
    return sectionHeader;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *groupsChoices = (_dishCombination==nil)?_dish.groupsChoices:_dishCombination.groupsChoices;
    if (groupsChoices.count > section) {
        GroupsChoice *gc = groupsChoices[section];
        return gc.choices.count;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SelectionCell *cell = [tableView dequeueReusableCellWithIdentifier:[SelectionCell description] forIndexPath:indexPath];
    NSArray *groupsChoices = (_dishCombination==nil)?_dish.groupsChoices:_dishCombination.groupsChoices;
    GroupsChoice *gc = groupsChoices[indexPath.section];
    cell.groupChoice = gc;
    cell.choice = gc.choices[indexPath.row];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SelectionCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.groupChoice.groupMaxSelect == 1) {
        if (cell.choice.isSelected)
            return;
        for (Choice *choice in cell.groupChoice.choices) {
            choice.isSelected = NO;
        }

        cell.choice.isSelected = YES;
        [tableView beginUpdates];
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
        [tableView endUpdates];
    }
    else {
        cell.choice.isSelected = !cell.choice.isSelected;
        [tableView beginUpdates];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [tableView endUpdates];
    }
    
    [self calculateUnitPrice];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)addCartBtnAct:(id)sender {
//    NSArray *array = [NSArray arrayWithObjects:_dish, nil];
    Dish *dish = [_dish copy];
    dish.dishCombinations = [NSArray array];
    dish.groupsChoices = [NSArray array];
    if (_dishCombination) {
//        NSArray *array = [NSArray arrayWithObjects:_dishCombination, nil];
        DishCombination *comb = [_dishCombination copy];
        comb.groupsChoices = [NSArray array];
        dish.dishCombinations = [NSArray arrayWithObjects:comb, nil];
    }
    NSArray *groupsChoices = (_dishCombination==nil)?_dish.groupsChoices:_dishCombination.groupsChoices;
    NSMutableArray *choices = [NSMutableArray array];
    for (GroupsChoice *gc in groupsChoices) {
        for (Choice *choice in gc.choices) {
            if (choice.isSelected) {
                [choices addObject:choice];
            }
        }
    }
    dish.choices = [NSArray arrayWithArray:choices];
    dish.quantity = [quantityField.text integerValue];
    dish.totalPrice = unitPrice*dish.quantity;
    dish.specialRequest = instructionField.text;
    
    SubmitOrderRequest *data = [SubmitOrderRequest instance];
    NSMutableArray *dishs = [NSMutableArray arrayWithArray:data.dishs];
    [dishs addObject:dish];
    data.dishs = dishs;
    
    [[data dictionary] writeToFile:BUSKETPLISTPATH atomically: YES];
    [TSMessage showNotificationWithTitle:@"Item successfully added to your busket" type:TSMessageNotificationTypeSuccess];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
