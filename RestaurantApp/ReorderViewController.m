//
//  ReorderViewController.m
//  RestaurantApp
//
//  Created by TM iMac on 1/9/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "ReorderViewController.h"
#import "Constants.h"

@interface ReorderViewController ()

@end

@implementation ReorderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = RestaurantName;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"side_menu"] style:UIBarButtonItemStylePlain target:appDelegate action:@selector(toggleSideMenu:)];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
