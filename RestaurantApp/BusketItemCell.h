//
//  BusketItemCell.h
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 2/6/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataModels.h"

@protocol BusketItemCellDelegate;
@interface BusketItemCell : UITableViewCell {
    
    __weak IBOutlet UILabel *quantityLabel;
    __weak IBOutlet UILabel *choiceLabel;
    __weak IBOutlet UILabel *priceLbl;
    __weak IBOutlet UILabel *nameLbl;
}

@property (nonatomic, retain) Dish *dish;
@property (weak, nonatomic) id<BusketItemCellDelegate> delegate;

@end



@protocol BusketItemCellDelegate <NSObject>
@optional
- (void)dishRemoveBtnAct:(BusketItemCell*)sender;

@end
