//
//  BasketViewController.m
//  RestaurantApp
//
//  Created by TM iMac on 1/9/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "BasketViewController.h"
#import "Constants.h"
#import "BusketItemCell.h"
#import "SubmitOrderRequest.h"
#import "DeliveryCollectionTimeResponse.h"
#import "ServiceView.h"
#import "APIManager.h"
#import "SavedAdressViewController.h"
#import "DiscountCell.h"
#import "UITextView+XibConfiguration.h"
#import "CheckoutViewController.h"

@interface BasketViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, DiscountCellDelegate, BusketItemCellDelegate> {
    SubmitOrderRequest *submitOrderRequ;
    __weak IBOutlet NSLayoutConstraint *itemTableHeightContraint;
    __weak IBOutlet UITableView *itemTable;
    DeliveryCollectionTimeResponse *timeResponse;
    __weak IBOutlet ServiceView *deliveryView;
    __weak IBOutlet ServiceView *collectionView;
    
    UIPickerView *timePickerView;
    __weak IBOutlet UITextField *addressField;
    __weak IBOutlet UITextField *timeField;
    NSArray<OfferDiscount *> *discountOffers;
    __weak IBOutlet UIView *topPlaceholderView;
    
    __weak IBOutlet UILabel *closingStatus;
    __weak IBOutlet UITextView *commentView;
    __weak IBOutlet UILabel *totalAmountLbl;
    __weak IBOutlet UILabel *deliveryFeeLbl;
    __weak IBOutlet UILabel *collectionTimeLbl;
    __weak IBOutlet UILabel *deliveryTimeLbl;
    OfferDiscount *selectedOffer;
    
    double totalPrice;//, delivery_fee, discount_amount;
//    NSString *offer_title, *discount_title, *order_for;
//    Address *selectedAddress;
    __weak IBOutlet UIStackView *deliveryAddressSection;

}

@end

@implementation BasketViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = RestaurantName;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"side_menu"] style:UIBarButtonItemStylePlain target:appDelegate action:@selector(toggleSideMenu:)];

    itemTable.estimatedRowHeight = itemTable.rowHeight;
    itemTable.rowHeight = UITableViewAutomaticDimension;
    itemTableHeightContraint.constant = 5000;
    
    timePickerView = [[UIPickerView alloc] init];
    timePickerView.dataSource = self;
    timePickerView.delegate = self;
    timeField.inputView = timePickerView;

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    topPlaceholderView.hidden = YES;
    collectionTimeLbl.hidden =YES;
    submitOrderRequ = [SubmitOrderRequest instance];
    RestaurantData *restaurantData = [RestaurantData sharedData];
    deliveryView.timeLabel.text = restaurantData.deliveryTimeText;
    collectionView.timeLabel.text = restaurantData.collectionTimeText;
    [self setServiceForView:[submitOrderRequ.orderFor isEqualToString:@"Collection"]?collectionView:deliveryView];
    [self showAddressOnLabel];
    topPlaceholderView.hidden = submitOrderRequ.dishs.count>0;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    if (selectedAddress) {
//        submitOrderRequ.delivery_collection_address = selectedAddress;
//    }
    submitOrderRequ.finalComment = commentView.text;
    [[submitOrderRequ dictionary] writeToFile:BUSKETPLISTPATH atomically: YES];

}

- (IBAction)tapOnServiceView:(UITapGestureRecognizer *)sender {
    ServiceView *view = (ServiceView*)sender.view;
    if (view.selectBtn.selected) {
        return;
    }
    [self setServiceForView:view];
}


- (void)setServiceForView:(ServiceView*)view {
    submitOrderRequ.orderFor = view.serviceLabel.text;
    RestaurantData *restaurantData = [RestaurantData sharedData];
    if ([view isEqual:deliveryView] && !restaurantData.isDelivery) {
        [TSMessage showNotificationWithTitle:@"This restaurant currently not delivering." type:TSMessageNotificationTypeWarning];
        return;
    }
    ServiceView *selectedView = view;
    ServiceView *deselectedView = [selectedView isEqual:deliveryView]?collectionView:deliveryView;
    selectedView.backgroundColor = [UIColor colorWithRed:0.29 green:0.80 blue:0.38 alpha:1.00];
    deselectedView.backgroundColor= selectedView.serviceLabel.textColor = selectedView.timeLabel.textColor = [UIColor whiteColor];
    if([selectedView isEqual:collectionView])
    {
        deliveryTimeLbl.hidden = YES;
        collectionTimeLbl.hidden =NO;
    }
    else{
        deliveryTimeLbl.hidden = NO;
        collectionTimeLbl.hidden =YES;
    }
    selectedView.selectBtn.selected = YES;
    deselectedView.selectBtn.selected = NO;
    deselectedView.serviceLabel.textColor = deselectedView.timeLabel.textColor = [UIColor darkTextColor];
    [self getTimeForService:view];
}

- (void)getTimeForService:(ServiceView*)view {
    deliveryAddressSection.hidden = [view isEqual:collectionView];

    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:submitOrderRequ.orderFor forKey:@"order_for"];
    [param setValue:submitOrderRequ.deliveryCollectionAddress.postCode forKey:@"delivery_post_code"];
    
    [[APIManager sharedManager] executePOSTRequestWith:[NSString stringWithFormat:@"GetDeliveryCollectionTimes.php?restaurant_id=%@",RestaurantId] Parameters:param ForSuccess:^(id response) {
        timeResponse = [DeliveryCollectionTimeResponse objectFromDictionary:[response valueForKey:@"Message"]];
        NSInteger index = 0, selected = 0;
        for (DeliveryCollectionTime *tm in timeResponse.deliveryCollectionTimes) {
            if ([tm.dateTime isEqualToString:submitOrderRequ.deliveryCollectionDateTime]) {
                selected = index;
                break;
            }
            index++;
        }
        [self selectDeliveryCollectionTimeForIndex:selected];
        [self getOfferDiscountsForService:view];
    } ForFail:^(NSString *error) {
        [TSMessage showNotificationWithTitle:error type:TSMessageNotificationTypeError];
    }];
}

- (void)getOfferDiscountsForService:(ServiceView*)view {
//    selectedOffer = nil;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:submitOrderRequ.orderFor forKey:@"order_for"];
    [param setValue:[submitOrderRequ.dishs dictionary] forKey:@"dishs"];
//    NSLog(@"%@",param);
    [[APIManager sharedManager] executePOSTRequestWith:[NSString stringWithFormat:@"GetOffersDiscounts.php?restaurant_id=%@",RestaurantId] Parameters:param ForSuccess:^(id response) {
        NSDictionary *data = [response valueForKey:@"Message"];
        NSArray *discounts = [OfferDiscount objectFromDictionary:[data valueForKey:@"discounts"]];
        discountOffers = [discounts arrayByAddingObjectsFromArray:[OfferDiscount objectFromDictionary:[data valueForKey:@"offers"]]];
        [self reloadItemTable];
    } ForFail:^(NSString *error) {
        [TSMessage showNotificationWithTitle:error type:TSMessageNotificationTypeError];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)reloadItemTable {
    NSArray *totalPrices = [submitOrderRequ.dishs valueForKey:@"totalPrice"];
    totalPrice = [[totalPrices valueForKeyPath: @"@sum.self"] doubleValue];
    RestaurantData *restaurantData = [RestaurantData sharedData];
    submitOrderRequ.deliveryFee = restaurantData.deliveryFee;
    if (selectedOffer != nil) {
        if (selectedOffer.offerTitle.length > 0) {
            submitOrderRequ.offerTitle = selectedOffer.offerTitle;
            submitOrderRequ.discountTitle = @"";
            submitOrderRequ.offerTitle = @"";
            submitOrderRequ.discountAmount = 0;
        }
        else {
            if (selectedOffer.offerTitle.length > 0) {
                submitOrderRequ.offerTitle = selectedOffer.offerTitle;
                submitOrderRequ.discountTitle = @"";
            }
            else {
                submitOrderRequ.discountTitle = selectedOffer.discountTitle;
                submitOrderRequ.offerTitle = @"";
            }

            if ([selectedOffer.discountType isEqualToString:@"percent"]) {
                submitOrderRequ.discountAmount = totalPrice*selectedOffer.discountAmount/100;
            }
            else {
                submitOrderRequ.discountAmount = selectedOffer.discountAmount;
            }
//            NSLog(@"total =  %.2f,  discount = %.2f",totalPrice,submitOrderRequ.discountAmount);
            totalPrice -= submitOrderRequ.discountAmount;
        }
    }
    NSString *fee = @"Free";
    if (submitOrderRequ.deliveryFee > 0 && deliveryView.selectBtn.selected) {
        fee = [NSString stringWithFormat:@"£%.2f", submitOrderRequ.deliveryFee];
        totalPrice += submitOrderRequ.deliveryFee;
    }
    deliveryFeeLbl.text = fee;
    totalAmountLbl.text = [NSString stringWithFormat:@"£%.2f", totalPrice];

    [itemTable reloadData];
    [itemTable layoutIfNeeded];
    itemTableHeightContraint.constant = itemTable.contentSize.height;// + 0*totalChoices - 0*66;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:[CheckoutViewController description]]) {
        CheckoutViewController *vc = [segue destinationViewController];
        vc.totalPrice = totalPrice;
        if (selectedOffer) {
            vc.selectedOffer = selectedOffer;
        }
    }
}


- (void)selectDeliveryCollectionTimeForIndex:(NSInteger)index {
    if (timeResponse.deliveryCollectionTimes.count > index) {
        DeliveryCollectionTime *div = timeResponse.deliveryCollectionTimes[index];
        timeField.text = div.dateTime;
    }
    else if (timeResponse.deliveryCollectionTimes.count > 0) {
        DeliveryCollectionTime *div = timeResponse.deliveryCollectionTimes[0];
        timeField.text = div.dateTime;
    }
    submitOrderRequ.deliveryCollectionDateTime = timeField.text;
}

- (IBAction)showTimeSlots {
    [timeField becomeFirstResponder];
}

- (IBAction)goForSavedAddress {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Address" bundle:nil];
    SavedAdressViewController *vc = [sb instantiateViewControllerWithIdentifier:[SavedAdressViewController description]];
    vc.didSelectAddress = ^(Address *address){
        submitOrderRequ.deliveryCollectionAddress = address;
        [self showAddressOnLabel];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showAddressOnLabel {
    if (submitOrderRequ.deliveryCollectionAddress) {
        NSString *line;
        if (submitOrderRequ.deliveryCollectionAddress.addressLine1) {
            line = submitOrderRequ.deliveryCollectionAddress.addressLine1;
        }

        if (submitOrderRequ.deliveryCollectionAddress.addressLine2.length>0) {
            line = [NSString stringWithFormat:@"%@ %@",line, submitOrderRequ.deliveryCollectionAddress.addressLine2];
        }
        if (submitOrderRequ.deliveryCollectionAddress.townCity.length > 0) {
            line = [NSString stringWithFormat:@"%@ %@",line, submitOrderRequ.deliveryCollectionAddress.townCity];
        }
        if (submitOrderRequ.deliveryCollectionAddress.postCode.length > 0) {
            line = [NSString stringWithFormat:@"%@ %@",line, submitOrderRequ.deliveryCollectionAddress.postCode];
        }
        addressField.text = line;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == addressField) {
        [self goForSavedAddress];
        return NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == timeField) {
        [self selectDeliveryCollectionTimeForIndex:[timePickerView selectedRowInComponent:0]];
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    [textView updatePlaceholderVisibility];
}

- (IBAction)addMoreItem:(id)sender {
    [[TabBarController sharedController] setSelectedIndex:0];
}

- (IBAction)checkoutBtnAct:(id)sender {
    if ([submitOrderRequ.orderFor isEqualToString:@"Delivery"] && addressField.text.length <1) {
        [TSMessage showNotificationWithTitle:@"Please add delivery address" type:TSMessageNotificationTypeError];
        return;
    }
    [self performSegueWithIdentifier:[CheckoutViewController description] sender:nil];
}

#pragma mark - UIPickerViewDelegate, UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == timePickerView) {
        return timeResponse.deliveryCollectionTimes.count;
    }
    
    return 0;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView == timePickerView) {
        DeliveryCollectionTime *div = timeResponse.deliveryCollectionTimes[row];
        return div.dateTime;
    }
    
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView == timePickerView) {
        [self selectDeliveryCollectionTimeForIndex:row];
    }
}

- (void)dishRemoveBtnAct:(BusketItemCell *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Remove Item!" message:[NSString stringWithFormat:@"Are you sure want to delete '%@' from your basket?", sender.dish.dishName] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [submitOrderRequ.dishs removeObject:sender.dish];
        [self reloadItemTable];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)discountAddBtnAct:(DiscountCell *)sender {
    for (OfferDiscount *offer in discountOffers) {
        offer.isSelected = NO;
    }
    selectedOffer = sender.discount;
    selectedOffer.isSelected = YES;
    [self reloadItemTable];
}

- (void)discountRemoveBtnAct:(DiscountCell *)sender {
    selectedOffer.isSelected = NO;
    selectedOffer = nil;
    [self reloadItemTable];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return submitOrderRequ.dishs.count;
            break;
        case 1:
            return (selectedOffer==nil)?discountOffers.count:1;
            break;
        default:
            return 0;
        break;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        BusketItemCell *cell = [tableView dequeueReusableCellWithIdentifier:[BusketItemCell description] forIndexPath:indexPath];
        cell.delegate = self;
        cell.dish = submitOrderRequ.dishs[indexPath.row];
        return cell;
    }
    else if (indexPath.section == 1) {
        DiscountCell *cell = [tableView dequeueReusableCellWithIdentifier:[DiscountCell description] forIndexPath:indexPath];
        cell.delegate = self;
        if (selectedOffer == nil) {
            cell.discount = discountOffers[indexPath.row];
            cell.amountLbl.hidden = YES;
        }
        else {
            cell.discount = selectedOffer;
            cell.amountLbl.hidden = NO;
            cell.amountLbl.text = [NSString stringWithFormat:@"£%.2f", submitOrderRequ.discountAmount];
        }
        return cell;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DeliveryCell" forIndexPath:indexPath];
    return cell;
}

@end
