//
//  ChangePasswordViewController.m
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 1/10/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController () {
    
    __weak IBOutlet UITextField *newPasswordField;
    __weak IBOutlet UITextField *currentPassField;
}

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Change Password";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)changeBtnAct:(id)sender {
    
}

@end
