//
//  User.h
//  RestaurantApp
//
//  Created by TM iMac on 1/10/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject


+ (User *)sharedUser;

@property (nonatomic, assign) NSInteger customerId;
@property (nonatomic, retain) NSString *accessToken, *deviceToken, *anniversaryDate, *anniversaryMonth, *dateOfBirth, *email, *firstName, *gender, *lastName, *mobileNumber, *monthOfBirth;

- (void)setShared:(User*)user;

@end



@interface Address : NSObject


@property (nonatomic, assign) NSInteger addressId;
@property (nonatomic, retain) NSString *addressLabel, *addressLine1, *addressLine2, *townCity, *county, *postCode;


@end
