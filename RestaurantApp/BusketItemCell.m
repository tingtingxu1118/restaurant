//
//  BusketItemCell.m
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 2/6/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "BusketItemCell.h"

@implementation BusketItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDish:(Dish *)dish {
    _dish = dish;
    quantityLabel.text = [@(dish.quantity) stringValue];
    NSString *name = dish.dishName;
    if (dish.dishCombinations.count > 0) {
        DishCombination *dc = [dish.dishCombinations firstObject];
        name = [name stringByAppendingString:[NSString stringWithFormat:@", %@", dc.combName]];
    }
    nameLbl.text = name;
    name = @"";
    for (Choice *ch in dish.choices) {
        if (name.length > 0) {
            name = [name stringByAppendingString:@"\n"];
        }
        name = [name stringByAppendingString:[NSString stringWithFormat:@"+%@", ch.choiceName]];
    }
//    NSLog(@"Name:%@",name);
    choiceLabel.text = name;
    
    priceLbl.text = [NSString stringWithFormat:@"£%.2f", dish.totalPrice];
}

- (IBAction)deleteBtnAct:(id)sender {
    if ([self.delegate respondsToSelector:@selector(dishRemoveBtnAct:)]) {
        [self.delegate dishRemoveBtnAct:self];
    }
}

@end
