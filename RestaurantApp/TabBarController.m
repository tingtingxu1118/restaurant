//
//  TabBarController.m
//  RestaurantApp
//
//  Created by TM iMac on 1/10/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "TabBarController.h"
#import "Constants.h"

@interface TabBarController ()<UITabBarControllerDelegate>

@end

static TabBarController* shared = nil;

@implementation TabBarController

+ (TabBarController*)sharedController {
    if (!shared) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        shared = [storyboard instantiateViewControllerWithIdentifier:[TabBarController description]];
    }
    return shared;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    NSInteger indx = [self.viewControllers indexOfObject:viewController];
    NSLog(@"%ld", (long)indx);
    if (indx > 0 && indx < 3 && ![USERDEFAULTS boolForKey:kLOGINKEY]) {
        [appDelegate goForLogin];
        return NO;
    }
    return YES;
}
@end
