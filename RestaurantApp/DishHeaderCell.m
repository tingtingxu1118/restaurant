//
//  DishHeaderCell.m
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 3/30/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "DishHeaderCell.h"
#import <Masonry.h>
#import "APIManager.h"

@interface DishHeaderCell()<ChoiceCellDelegate>

@property (nonatomic, strong) MASConstraint *topConstraint;

@end

@implementation DishHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    descriptionLbl.tag = 3500;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDish:(Dish *)dish {
    _dish = dish;
    addBtn.hidden = priceLbl.hidden = (dish.dishCombinations.count > 0);
    priceLbl.text = [NSString stringWithFormat:@"£%.2f",dish.dishPrice];
    nameLbl.text = dish.dishName;
    
    NSArray *dishFavourites = [[RestaurantData sharedData].dishFavourites valueForKeyPath:@"dishId"];
    favouriteBtn.selected = [dishFavourites containsObject:@(_dish.dishId)];
//    descriptionLbl.text = dish.dishDescription;
    NSMutableAttributedString * attrString = [[NSMutableAttributedString alloc] initWithData:[dish.dishDescription dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    [attrString addAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13], NSForegroundColorAttributeName : descriptionLbl.textColor} range:NSMakeRange(0, attrString.length)];
    descriptionLbl.attributedText = attrString;
    
    [self.topConstraint uninstall];
    for (id view in self.contentView.subviews) {
        if ([view isKindOfClass:[ChoiceCellView class]]) {
            [(ChoiceCellView*)view removeFromSuperview];
        }
    }
    if (_dish.dishCombinations.count > 0) {
        NSInteger indx = 0;
        for (DishCombination *comb in _dish.dishCombinations) {
            ChoiceCellView *view = [ChoiceCellView viewWithCombination:comb];
            view.delegate = self;
            view.dish = _dish;
            view.tag = 3501+indx;
            [self.contentView addSubview:view];
            view.translatesAutoresizingMaskIntoConstraints = NO;
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                UIView *prev = [self.contentView viewWithTag:(3500+indx)];
                make.top.equalTo(prev.mas_bottom).with.offset(0);
                make.leading.equalTo(self.contentView).with.offset(0);
                make.trailing.equalTo(self.contentView).with.offset(0);
                if (indx == _dish.dishCombinations.count-1) {
                    self.topConstraint = make.bottomMargin.equalTo(self.contentView.mas_bottomMargin).with.offset(0);
                }
            }];
            indx++;
        }
    }
    else {
        [descriptionLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            self.topConstraint = make.bottomMargin.equalTo(self.contentView.mas_bottomMargin).with.offset(0);
        }];
    }
}

- (void)goForAddItem:(ChoiceCellView *)sender {
    if ([self.delegate respondsToSelector:@selector(goForAddItem:)]) {
        [self.delegate goForAddItem:sender];
    }
}

- (IBAction)addButtonAct:(id)sender {
    if ([self.delegate respondsToSelector:@selector(goForAddItem:)]) {
        [self.delegate goForAddItem:self.dish];
    }
}

- (IBAction)favouriteBtnAct:(UIButton *)sender {
    if ([USERDEFAULTS boolForKey:kLOGINKEY]) {
        NSString *urlString = [NSString stringWithFormat:@"%@.php?restaurant_id=%@&customer_id=%ld", sender.selected?@"RemoveFav":@"AddFav", RestaurantId, (long)[User sharedUser].customerId];
        [[APIManager sharedManager] executePOSTRequestWith:urlString Parameters:@{@"dish_id": @(_dish.dishId)} ForSuccess:^(id response) {
            if (sender.selected) {
                sender.selected = NO;
                RestaurantData *sharedData = [RestaurantData sharedData];
                NSArray *fav = [sharedData.dishFavourites filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.dishId != %d", _dish.dishId]];
                sharedData.dishFavourites = [NSArray arrayWithArray:fav];
            } else {
                sender.selected = YES;
                RestaurantData *sharedData = [RestaurantData sharedData];
                NSMutableArray *fav = [NSMutableArray arrayWithArray:sharedData.dishFavourites];
                [fav addObject:_dish];
                sharedData.dishFavourites = [NSArray arrayWithArray:fav];
            }
            [TSMessage showNotificationWithTitle:[response valueForKey:@"Message"] type:TSMessageNotificationTypeSuccess];
        } ForFail:^(NSString *error) {
            [TSMessage showNotificationWithTitle:error type:TSMessageNotificationTypeError];
        }];
    }
}

@end
