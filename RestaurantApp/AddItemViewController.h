//
//  AddItemViewController.h
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 1/27/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectionCell.h"

@interface AddItemViewController : UIViewController

@property (nonatomic, copy) Dish *dish;
@property (nonatomic, copy) DishCombination *dishCombination;

@end
