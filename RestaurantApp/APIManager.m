//
//  APIManager.m
//  RestaurantApp
//
//  Created by TM iMac on 11/29/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import "APIManager.h"
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>

static APIManager* sharedManger = nil;

@implementation APIManager {
    AFHTTPSessionManager *manager;
    
}

+ (APIManager*)sharedManager {
    if (!sharedManger) {
        sharedManger = [[APIManager alloc] initWithBaseURL:[NSURL URLWithString:[Environment baseURLstring]]];
    }
    return sharedManger;
}

- (instancetype)initWithBaseURL:(NSURL *)url{
    if (self = [super init]) {
        manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"text/html", @"application/json", nil]];
//        , @"application/x-www-form-urlencoded"
    }
    
    return self;
}

- (void)executePOSTRequestWith:(NSString*)urlString Parameters:(NSDictionary*)parameters ForSuccess:(SuccessCompletionHandler)success ForFail:(FailureCompletionHandler)failure {
    NSString *token = [USERDEFAULTS boolForKey:kLOGINKEY]?[User sharedUser].accessToken:UserToken;
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Usertoken"];
//    NSLog(@"token: %@",token);
    [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString *(NSURLRequest *request, id parameters, NSError * __autoreleasing * error) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:error];
        NSString *argString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return argString;
    }];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:appDelegate.window animated:YES];
    [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [hud hideAnimated:YES];
//        NSLog(@"Success %@ : %@",urlString, responseObject);
        if ([[responseObject valueForKey:@"Success"] boolValue]) {
            success(responseObject);
        }
        else {
            failure([responseObject valueForKey:@"Message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [hud hideAnimated:YES];
        failure(error.localizedDescription);
        NSLog(@"Failure %@ : %@",urlString, error.localizedDescription);
    }];
}

- (void)executeGETRequestWith:(NSString*)urlString Parameters:(NSDictionary*)parameters ForSuccess:(SuccessCompletionHandler)success ForFail:(FailureCompletionHandler)failure {
    [manager.requestSerializer setValue:[USERDEFAULTS boolForKey:kLOGINKEY]?[User sharedUser].accessToken:UserToken forHTTPHeaderField:@"Usertoken"];
    [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString *(NSURLRequest *request, id parameters, NSError * __autoreleasing * error) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:error];
        NSString *argString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return argString;
    }];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:appDelegate.window animated:YES];
    [manager GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [hud hideAnimated:YES];
//        NSLog(@"Success %@ : %@",urlString, responseObject);
        if ([[responseObject valueForKey:@"Success"] boolValue]) {
            success(responseObject);
        }
        else {
            failure([responseObject valueForKey:@"Message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [hud hideAnimated:YES];
        failure(error.localizedDescription);
        NSLog(@"Failure %@ : %@",urlString, error.localizedDescription);
    }];
}

- (void)executePUTRequestWith:(NSString*)urlString Parameters:(NSDictionary*)parameters ForSuccess:(SuccessCompletionHandler)success ForFail:(FailureCompletionHandler)failure {
    [manager.requestSerializer setValue:[USERDEFAULTS boolForKey:kLOGINKEY]?[User sharedUser].accessToken:UserToken forHTTPHeaderField:@"Usertoken"];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:appDelegate.window animated:YES];
    [manager PUT:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [hud hideAnimated:YES];
        NSLog(@"Success %@ : %@",urlString, responseObject);
        if ([[responseObject valueForKey:@"Success"] boolValue]) {
            success(responseObject);
        }
        else {
            failure([responseObject valueForKey:@"Message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [hud hideAnimated:YES];
        failure(error.localizedDescription);
        NSLog(@"Failure %@ : %@",urlString, error.localizedDescription);
    }];
}

- (void)executeDELETERequestWith:(NSString*)urlString Parameters:(NSDictionary*)parameters ForSuccess:(SuccessCompletionHandler)success ForFail:(FailureCompletionHandler)failure {
    [manager.requestSerializer setValue:[USERDEFAULTS boolForKey:kLOGINKEY]?[User sharedUser].accessToken:UserToken forHTTPHeaderField:@"Usertoken"];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:appDelegate.window animated:YES];
    [manager DELETE:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [hud hideAnimated:YES];
//        NSLog(@"Success %@ : %@",urlString, responseObject);
        if ([[responseObject valueForKey:@"Success"] boolValue]) {
            success(responseObject);
        }
        else {
            failure([responseObject valueForKey:@"Message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [hud hideAnimated:YES];
        failure(error.localizedDescription);
        NSLog(@"Failure %@ : %@",urlString, error.localizedDescription);
    }];
}
@end
