//
//  SelectionCell.m
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 2/1/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "SelectionCell.h"

@implementation SelectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setChoice:(Choice *)choice {
    _choice = choice;
    if (_groupChoice.groupMaxSelect == 1) {
        selectBtn.image = [UIImage imageNamed:choice.isSelected?@"bullet_active":@"bullet_deactive"];
    }
    else {
        selectBtn.image = [UIImage imageNamed:choice.isSelected?@"check_active":@"check_deactive"];
    }
    nameLbl.text = choice.choiceName;
    if (choice.price > 0) {
        nameLbl.text = [NSString stringWithFormat:@"%@ (£%.2f)", choice.choiceName, choice.price];
    }
}
@end
