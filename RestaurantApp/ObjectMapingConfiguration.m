//
//  ObjectMapingConfiguration.m
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 2/20/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "ObjectMapingConfiguration.h"
#import "Constants.h"
#import "SubmitOrderRequest.h"

@implementation ObjectMapingConfiguration

+ (InCodeMappingProvider*)getInCodeMappingProvider {
    [[ObjectMapper sharedInstance] setNormalizeDictionary:YES];
    InCodeMappingProvider *inCodeMappingProvider = [[InCodeMappingProvider alloc] init];
    [inCodeMappingProvider mapFromDictionaryKey:@"Monday" toPropertyKey:@"Monday" withObjectType:[TimeSlot class] forClass:[DailyHours class]];
    [inCodeMappingProvider mapFromDictionaryKey:@"Tuesday" toPropertyKey:@"Tuesday" withObjectType:[TimeSlot class] forClass:[DailyHours class]];
    [inCodeMappingProvider mapFromDictionaryKey:@"Wednesday" toPropertyKey:@"Wednesday" withObjectType:[TimeSlot class] forClass:[DailyHours class]];
    [inCodeMappingProvider mapFromDictionaryKey:@"Thursday" toPropertyKey:@"Thursday" withObjectType:[TimeSlot class] forClass:[DailyHours class]];
    [inCodeMappingProvider mapFromDictionaryKey:@"Friday" toPropertyKey:@"Friday" withObjectType:[TimeSlot class] forClass:[DailyHours class]];
    [inCodeMappingProvider mapFromDictionaryKey:@"Saturday" toPropertyKey:@"Saturday" withObjectType:[TimeSlot class] forClass:[DailyHours class]];
    [inCodeMappingProvider mapFromDictionaryKey:@"Sunday" toPropertyKey:@"Sunday" withObjectType:[TimeSlot class] forClass:[DailyHours class]];
    
    [inCodeMappingProvider mapFromDictionaryKey:@"allergen_Infos" toPropertyKey:@"allergenInfos" withObjectType:[DishKey class] forClass:[Dish class]];
    [inCodeMappingProvider mapFromDictionaryKey:@"category_id" toPropertyKey:@"category.categoryId" forClass:[Dish class]];
    [inCodeMappingProvider mapFromDictionaryKey:@"category_name" toPropertyKey:@"category.categoryName" forClass:[Dish class]];
    
    
    [inCodeMappingProvider mapFromDictionaryKey:@"delivery_radius" toPropertyKey:@"deliveryRadius" withObjectType:[DeliveryPostcode class] forClass:[RestaurantData class]];
    
    [inCodeMappingProvider mapFromDictionaryKey:@"dish_favourites" toPropertyKey:@"dishFavourites" withObjectType:[Dish class] forClass:[RestaurantData class]];
    
    [inCodeMappingProvider mapFromDictionaryKey:@"choice_price" toPropertyKey:@"price" forClass:[Choice class]];
    [inCodeMappingProvider mapFromDictionaryKey:@"choice_id" toPropertyKey:@"id" forClass:[Choice class]];
    
    ////////////\\\\\\\\\\\\\\\\\\\\//////////mapFromPropertyKey\\\\\\\\\\//////////\\\\\\\\\
    [inCodeMappingProvider mapFromPropertyKey:@"addressId" toDictionaryKey:@"address_id" forClass:[Address class]];
    [inCodeMappingProvider mapFromPropertyKey:@"addressLabel" toDictionaryKey:@"address_label" forClass:[Address class]];
    [inCodeMappingProvider mapFromPropertyKey:@"addressLine1" toDictionaryKey:@"address_line1" forClass:[Address class]];
    [inCodeMappingProvider mapFromPropertyKey:@"addressLine2" toDictionaryKey:@"address_line2" forClass:[Address class]];
    [inCodeMappingProvider mapFromPropertyKey:@"postCode" toDictionaryKey:@"post_code" forClass:[Address class]];
    [inCodeMappingProvider mapFromPropertyKey:@"townCity" toDictionaryKey:@"town_city" forClass:[Address class]];
    
    
    [inCodeMappingProvider mapFromPropertyKey:@"anniversaryDate" toDictionaryKey:@"anniversary_date" forClass:[User class]];
    [inCodeMappingProvider mapFromPropertyKey:@"anniversaryMonth" toDictionaryKey:@"anniversary_month" forClass:[User class]];
    [inCodeMappingProvider mapFromPropertyKey:@"dateOfBirth" toDictionaryKey:@"date_of_birth" forClass:[User class]];
    [inCodeMappingProvider mapFromPropertyKey:@"firstName" toDictionaryKey:@"first_name" forClass:[User class]];
    [inCodeMappingProvider mapFromPropertyKey:@"lastName" toDictionaryKey:@"last_name" forClass:[User class]];
    [inCodeMappingProvider mapFromPropertyKey:@"mobileNumber" toDictionaryKey:@"mobile_number" forClass:[User class]];
    [inCodeMappingProvider mapFromPropertyKey:@"monthOfBirth" toDictionaryKey:@"month_of_birth" forClass:[User class]];

    [inCodeMappingProvider mapFromPropertyKey:@"specialRequest" toDictionaryKey:@"special_request" forClass:[Dish class]];
    [inCodeMappingProvider mapFromPropertyKey:@"totalPrice" toDictionaryKey:@"total_price" forClass:[Dish class]];
    [inCodeMappingProvider mapFromPropertyKey:@"dishCombinations" toDictionaryKey:@"dish_combinations" forClass:[Dish class]];
    [inCodeMappingProvider mapFromPropertyKey:@"dishPrice" toDictionaryKey:@"dish_price" forClass:[Dish class]];
    [inCodeMappingProvider mapFromPropertyKey:@"dishDescription" toDictionaryKey:@"dish_description" forClass:[Dish class]];
    [inCodeMappingProvider mapFromPropertyKey:@"dishName" toDictionaryKey:@"dish_name" forClass:[Dish class]];
    [inCodeMappingProvider mapFromPropertyKey:@"dishId" toDictionaryKey:@"dish_id" forClass:[Dish class]];
    [inCodeMappingProvider mapFromPropertyKey:@"dishDescription" toDictionaryKey:@"dish_description" forClass:[Dish class]];
    [inCodeMappingProvider mapFromPropertyKey:@"category.categoryName" toDictionaryKey:@"category_name" forClass:[Dish class]];
    [inCodeMappingProvider mapFromPropertyKey:@"category.categoryId" toDictionaryKey:@"category_id" forClass:[Dish class]];

    [inCodeMappingProvider mapFromPropertyKey:@"combPrice" toDictionaryKey:@"comb_price" forClass:[DishCombination class]];
    [inCodeMappingProvider mapFromPropertyKey:@"combDescription" toDictionaryKey:@"comb_description" forClass:[DishCombination class]];
    [inCodeMappingProvider mapFromPropertyKey:@"combName" toDictionaryKey:@"comb_name" forClass:[DishCombination class]];
    [inCodeMappingProvider mapFromPropertyKey:@"combId" toDictionaryKey:@"comb_id" forClass:[DishCombination class]];

    [inCodeMappingProvider mapFromPropertyKey:@"choiceName" toDictionaryKey:@"choice_name" forClass:[Choice class]];
    [inCodeMappingProvider mapFromPropertyKey:@"price" toDictionaryKey:@"price" forClass:[Choice class]];
    [inCodeMappingProvider mapFromPropertyKey:@"id" toDictionaryKey:@"id" forClass:[Choice class]];
   
    /*
     @property (nonatomic, retain) NSString *order_for, *final_comment, *payment_method, *payment_status, *offer_title, *discount_title;
     @property (nonatomic, retain) NSString *delivery_collection_date_time;
     @property (nonatomic, retain) NSMutableDictionary *customer_details;
     @property (nonatomic, retain) Address *delivery_collection_address;
     @property (nonatomic, assign) double delivery_fee, discount_amount;
     @property (nonatomic, retain) NSMutableArray *dishs;
     */
    
//    [inCodeMappingProvider mapFromDictionaryKey:@"order_for" toPropertyKey:@"order_for" forClass:[SubmitOrderRequest class]];
//    [inCodeMappingProvider mapFromDictionaryKey:@"final_comment" toPropertyKey:@"final_comment" forClass:[SubmitOrderRequest class]];
//    [inCodeMappingProvider mapFromDictionaryKey:@"payment_method" toPropertyKey:@"payment_method" forClass:[SubmitOrderRequest class]];
//    [inCodeMappingProvider mapFromDictionaryKey:@"payment_status" toPropertyKey:@"payment_status" forClass:[SubmitOrderRequest class]];
//    [inCodeMappingProvider mapFromDictionaryKey:@"offer_title" toPropertyKey:@"offer_title" forClass:[SubmitOrderRequest class]];
//    [inCodeMappingProvider mapFromDictionaryKey:@"discount_title" toPropertyKey:@"discount_title" forClass:[SubmitOrderRequest class]];
//    [inCodeMappingProvider mapFromDictionaryKey:@"delivery_collection_date_time" toPropertyKey:@"delivery_collection_date_time" forClass:[SubmitOrderRequest class]];
//    [inCodeMappingProvider mapFromDictionaryKey:@"customer_details" toPropertyKey:@"customer_details" forClass:[SubmitOrderRequest class]];
//    [inCodeMappingProvider mapFromDictionaryKey:@"delivery_collection_address" toPropertyKey:@"delivery_collection_address" forClass:[SubmitOrderRequest class]];
//    [inCodeMappingProvider mapFromDictionaryKey:@"delivery_fee" toPropertyKey:@"delivery_fee" forClass:[SubmitOrderRequest class]];
//    [inCodeMappingProvider mapFromDictionaryKey:@"discount_amount" toPropertyKey:@"discount_amount" forClass:[SubmitOrderRequest class]];
    
    [inCodeMappingProvider mapFromPropertyKey:@"orderFor" toDictionaryKey:@"order_for" forClass:[SubmitOrderRequest class]];
    [inCodeMappingProvider mapFromPropertyKey:@"finalComment" toDictionaryKey:@"final_comment" forClass:[SubmitOrderRequest class]];
    [inCodeMappingProvider mapFromPropertyKey:@"paymentMethod" toDictionaryKey:@"payment_method" forClass:[SubmitOrderRequest class]];
    [inCodeMappingProvider mapFromPropertyKey:@"paymentStatus" toDictionaryKey:@"payment_status" forClass:[SubmitOrderRequest class]];
    [inCodeMappingProvider mapFromPropertyKey:@"offerTitle" toDictionaryKey:@"offer_title" forClass:[SubmitOrderRequest class]];
    [inCodeMappingProvider mapFromPropertyKey:@"discountTitle" toDictionaryKey:@"discount_title" forClass:[SubmitOrderRequest class]];
    [inCodeMappingProvider mapFromPropertyKey:@"deliveryCollectionDateTime" toDictionaryKey:@"delivery_collection_date_time" forClass:[SubmitOrderRequest class]];
    [inCodeMappingProvider mapFromPropertyKey:@"customerDetails" toDictionaryKey:@"customer_details" forClass:[SubmitOrderRequest class]];
    [inCodeMappingProvider mapFromPropertyKey:@"deliveryCollectionAddress" toDictionaryKey:@"delivery_collection_address" forClass:[SubmitOrderRequest class]];
    [inCodeMappingProvider mapFromPropertyKey:@"deliveryFee" toDictionaryKey:@"delivery_fee" forClass:[SubmitOrderRequest class]];
    [inCodeMappingProvider mapFromPropertyKey:@"discountAmount" toDictionaryKey:@"discount_amount" forClass:[SubmitOrderRequest class]];

    return inCodeMappingProvider;
}

@end
