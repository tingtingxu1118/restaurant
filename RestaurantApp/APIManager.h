//
//  APIManager.h
//  RestaurantApp
//
//  Created by TM iMac on 11/29/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

typedef void (^SuccessCompletionHandler)(id response);
typedef void (^FailureCompletionHandler)(NSString *error);

@interface APIManager : NSObject

+ (APIManager*) sharedManager;
- (instancetype)initWithBaseURL:(NSURL *)url;
- (void)executePOSTRequestWith:(NSString*)urlString Parameters:(NSDictionary*)parameters ForSuccess:(SuccessCompletionHandler)success ForFail:(FailureCompletionHandler)failure;
- (void)executeGETRequestWith:(NSString*)urlString Parameters:(NSDictionary*)parameters ForSuccess:(SuccessCompletionHandler)success ForFail:(FailureCompletionHandler)failure;
- (void)executePUTRequestWith:(NSString*)urlString Parameters:(NSDictionary*)parameters ForSuccess:(SuccessCompletionHandler)success ForFail:(FailureCompletionHandler)failure;
- (void)executeDELETERequestWith:(NSString*)urlString Parameters:(NSDictionary*)parameters ForSuccess:(SuccessCompletionHandler)success ForFail:(FailureCompletionHandler)failure;

@end
