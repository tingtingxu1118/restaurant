//
//  SplashViewController.m
//  RestaurantApp
//
//  Created by TM iMac on 1/9/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "SplashViewController.h"
#import "APIManager.h"
#import "ObjectMapingConfiguration.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[ObjectMapper sharedInstance] setMappingProvider:[ObjectMapingConfiguration getInCodeMappingProvider]];

    // Do any additional setup after loading the view.
    if ([USERDEFAULTS boolForKey:kLOGINKEY]) {
        [[User sharedUser] setShared:[User objectFromDictionary:[USERDEFAULTS valueForKey:kUSERSHAREDKEY]]];
    }
    [self loadRestaurantMenu];
//    [appDelegate performSelector:@selector(goForLogin) withObject:nil afterDelay:1.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)loadRestaurantMenu {
    
//    NSMutableDictionary *param = [NSMutableDictionary dictionary];
//    [param setValue:RestaurantId forKey:@"restaurant_id"];
    [[APIManager sharedManager] executeGETRequestWith:[NSString stringWithFormat:@"GetMenuFinal.php?restaurant_id=%@",RestaurantId] Parameters:nil ForSuccess:^(id response) {
        //        CommonLoggingProvider *commonLoggingProvider = [[CommonLoggingProvider alloc] initWithLogLevel:LogLevelInfo];
        //        [[ObjectMapper sharedInstance] setLoggingProvider:commonLoggingProvider];
        
        [RestaurantData storeSharedDataWith:[response valueForKey:@"Message"]];
        //        NSLog(@"_restaurantData == %@",_restaurantData.deliveryHours);
        [self goForward];
    } ForFail:^(NSString *error) {
        [TSMessage showNotificationWithTitle:error type:TSMessageNotificationTypeError];
    }];
    
}

- (void)goForward {
    appDelegate.container = [MFSideMenuContainerViewController containerWithCenterViewController:[TabBarController sharedController] leftMenuViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"LeftMenuViewController"] rightMenuViewController:nil];
    [self.navigationController pushViewController:appDelegate.container animated:NO];
}

@end
