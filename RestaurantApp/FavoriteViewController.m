//
//  FavoriteViewController.m
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 8/10/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "FavoriteViewController.h"
#import "DishHeaderCell.h"
#import "DataModels.h"
#import "AddItemViewController.h"

@interface FavoriteViewController ()<UITableViewDelegate, UITableViewDataSource, DishHeaderDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation FavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"side_menu"] style:UIBarButtonItemStylePlain target:appDelegate action:@selector(toggleSideMenu:)];

    self.navigationItem.title = @"Favorite Dishes";
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tableView.estimatedRowHeight = 44;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    [_tableView registerNib:[UINib nibWithNibName:[DishHeaderCell description] bundle:nil] forCellReuseIdentifier:[DishHeaderCell description]];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [RestaurantData sharedData].dishFavourites.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DishHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:[DishHeaderCell description] forIndexPath:indexPath];
    cell.dish = [RestaurantData sharedData].dishFavourites[indexPath.row];
    cell.delegate = self;
    return cell;
}

- (void)goForAddItem:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Menu" bundle:nil];
    AddItemViewController *vc = [sb instantiateViewControllerWithIdentifier:[AddItemViewController description]];
    if ([sender isKindOfClass:[Dish class]]) {
        NSDictionary *dish = [(Dish*)sender dictionary];
        vc.dish = [Dish objectFromDictionary:dish];
    }
    [self.navigationController pushViewController:vc animated:YES];
}

@end
