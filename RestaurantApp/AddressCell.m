//
//  AddressCell.m
//  RestaurantApp
//
//  Created by TM iMac on 1/16/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "AddressCell.h"

@implementation AddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setAddress:(Address *)address {
    _address = address;
    titleLbl.text = address.addressLabel;
    if (address.addressLine2.length > 0) {
        lineLbl.text = [NSString stringWithFormat:@"%@, %@", address.addressLine1, address.addressLine2];
    }
    else {
        lineLbl.text = address.addressLine1;
    }
    cityLbl.text = [NSString stringWithFormat:@"%@, %@", address.townCity, address.postCode];
    countryLbl.text = address.county;
}

@end
