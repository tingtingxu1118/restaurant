//
//  DishTableViewController.m
//  RestaurantApp
//
//  Created by TM iMac on 12/18/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import "DishTableViewController.h"
#import "DishSectionHeader.h"
#import "ChoiceTableViewCell.h"
#import "AddItemViewController.h"

@interface DishTableViewController ()<ChoiceTableViewCellDelegate, DishSectionHeaderDelegate>

@end

@implementation DishTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];

    if (_dishs.count > 0) {
        Dish *dish = [_dishs firstObject];
        self.navigationItem.title = dish.category.categoryName;
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:[DishSectionHeader description] bundle:nil] forHeaderFooterViewReuseIdentifier:[DishSectionHeader description]];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.estimatedRowHeight = self.tableView.rowHeight;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)goForAddItem:(id)sender {
    [self performSegueWithIdentifier:[AddItemViewController description] sender:sender];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return _dishs.count;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    DishSectionHeader *sectionHeader = (DishSectionHeader*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:[DishSectionHeader description]];
    sectionHeader.separetor.backgroundColor = (section==0?[UIColor whiteColor]:[UIColor lightGrayColor]);
    sectionHeader.delegate = self;
    Dish *dish = _dishs[section];
    sectionHeader.dish = dish;
    return sectionHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    DishSectionHeader *sectionHeader = (DishSectionHeader*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:[DishSectionHeader description]];
    Dish *dish = _dishs[section];
    sectionHeader.dish = dish;
    return [sectionHeader layoutHeightForTableView:tableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    Dish *dish = _dishs[section];
    return dish.dishCombinations.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChoiceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ChoiceTableViewCell description] forIndexPath:indexPath];
    cell.delegate = self;
    cell.dish = _dishs[indexPath.section];
    cell.dishCombination = cell.dish.dishCombinations[indexPath.row];
    return cell;
}




#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:[AddItemViewController description]]) {
        AddItemViewController *vc = segue.destinationViewController;
        if ([sender isKindOfClass:[Dish class]]) {
//            vc.dish = (Dish*)sender;
            NSDictionary *dish = [(Dish*)sender dictionary];
            vc.dish = [Dish objectFromDictionary:dish];
//            NSLog(@"%@",dish);
        }
        else if ([sender isKindOfClass:[ChoiceTableViewCell class]]) {
            ChoiceTableViewCell *cell = (ChoiceTableViewCell*)sender;
            vc.dishCombination = [DishCombination objectFromDictionary:[cell.dishCombination dictionary]];
            vc.dish = [Dish objectFromDictionary:[cell.dish dictionary]];
        }
    }
}


@end
