//
//  DiscountCell.h
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 2/21/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OfferDiscount.h"

@protocol DiscountCellDelegate;
@interface DiscountCell : UITableViewCell {
    
    __weak IBOutlet UIButton *applyBtn;
    __weak IBOutlet UILabel *titleLabel;
}

@property (nonatomic) OfferDiscount *discount;
@property (weak, nonatomic) id<DiscountCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *amountLbl;

@end


@protocol DiscountCellDelegate <NSObject>
@optional
- (void)discountAddBtnAct:(DiscountCell*)sender;
- (void)discountRemoveBtnAct:(DiscountCell*)sender;
@end
