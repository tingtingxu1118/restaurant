//
//  User.m
//  RestaurantApp
//
//  Created by TM iMac on 1/10/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "User.h"

static User *Shared = nil;

@implementation User

+ (User*) sharedUser {
    if (Shared == nil) {
        Shared = [[User alloc] init];
        //        NSLog(@"creating user.............");
    }
    return Shared;
}

- (void)setShared:(User *)user {
    user.deviceToken = Shared.deviceToken;
    Shared = user;
}

@end





@implementation Address


@end
