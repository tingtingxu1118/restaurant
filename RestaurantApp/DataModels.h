//
//  DataModels.h
//  RestaurantApp
//
//  Created by TM iMac on 11/29/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OCMapper/OCMapper.h>
//#import <Motis/Motis.h>

@interface DishCategory : NSObject<NSCopying>
@property (nonatomic, copy) NSString *categoryName;
@property (nonatomic, assign) NSInteger categoryId;
@property (nonatomic, copy) NSArray *dishs;


@end


@interface DailyHours : NSObject<NSCopying>
@property (nonatomic, strong) NSArray *Monday, *Tuesday, *Wednesday, *Thursday, *Friday, *Saturday, *Sunday;
@end


@interface TimeSlot : NSObject
@property (nonatomic, strong) NSString *openTime, *closeTime;
@end


@interface DeliveryPostcode : NSObject
@property (nonatomic, strong) NSString *deliveryPostCode;
@property (nonatomic, assign) NSInteger deliveryTime, minimumOrderAmount, deliveryRadiusMile;
@property (nonatomic) double deliveryFee;

@end


@interface DiscountException : NSObject
@property (nonatomic, strong) NSString *categoryId, *dishId, *dishCombId;
@end


@interface Discount : NSObject
@property (nonatomic, strong) NSString *discountTitle, *discountDescription, *discountType, *discountExceptionText, *discountFor, *discountDay;
@property (nonatomic, assign) NSInteger discountAmount, discountOrderAmount;
@property (nonatomic, strong) DiscountException *discountExceptionId;
@property (nonatomic, strong) NSDate *discountStartDate, *discountEndDate;
@end


@interface Offer : NSObject
@property (nonatomic, strong) NSString *offerTitle, *offerDescription, *offerFor, *offerDay;
@property (nonatomic, assign) NSInteger offerOrderAmount;
@property (nonatomic, strong) NSDate *offerStartDate, *offerEndDate;
@end


@interface RestaurantPaymentType : NSObject
@property (nonatomic, strong) NSString *name, *value;
@end

@interface DishKey : NSObject
@property (nonatomic, strong) NSString *keyText, *image;
@end


@interface Choice : NSObject<NSCopying>
@property (nonatomic, copy) NSString *choiceName;
@property (nonatomic, assign) NSInteger id;
@property (nonatomic, assign) double price;
@property (nonatomic, assign) BOOL isSelected;


@end


@interface GroupsChoice : NSObject<NSCopying>
@property (nonatomic, copy) NSString *groupName, *groupDescription;
@property (nonatomic, assign) NSInteger groupId, groupMinSelect, groupMaxSelect, groupIncludeSelect;
@property (nonatomic, copy) NSArray *choices;


@end


@interface DishCombination : NSObject<NSCopying>
@property (nonatomic, copy) NSString *combName, *combDescription;
@property (nonatomic, assign) NSInteger combId;
@property (nonatomic, assign) double combPrice;
@property (nonatomic, copy) NSArray *groupsChoices;


@end


@interface Dish : NSObject<NSCopying>
@property (nonatomic, copy) NSString *dishName, *dishDescription, *specialRequest;
@property (nonatomic, retain) DishCategory *category;
@property (nonatomic, assign) double dishPrice, totalPrice;
@property (nonatomic, assign) NSInteger dishId, cuisineId, quantity;
@property (nonatomic, copy) NSArray *dishCombinations, *groupsChoices, *dishKeys, *allergenInfos;

@property (nonatomic, copy) NSArray *choices;

- (id)copyWithZone:(NSZone*)zone;

@end


@interface RestaurantData : NSObject
@property (nonatomic, copy) NSString *restaurantLogo, *collectionTime, *deliveryTimeText, *collectionTimeText;
@property (nonatomic, assign) NSInteger restaurantId;
@property (nonatomic, assign) BOOL status, isTableBooking, isDelivery, isCollection;
@property (nonatomic, copy) NSArray *restaurantPaymentTypes, *offers, *discounts, *dishFavourites, *dishs, *dishCategorys;
@property (nonatomic, retain) DailyHours *deliveryHours, *businessHours;
@property (nonatomic, copy) NSArray *deliveryRadius, *deliveryPostcodes;
@property (nonatomic) double deliveryFee;

+ (RestaurantData *)sharedData;
+ (void)storeSharedDataWith:(NSDictionary*)info;

@end
