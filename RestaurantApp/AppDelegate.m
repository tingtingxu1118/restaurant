//
//  AppDelegate.m
//  RestaurantApp
//
//  Created by TM iMac on 11/2/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import <TSMessages/TSMessage.h>
#import "Reachability.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import <UserNotifications/UserNotifications.h>
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate () <UNUserNotificationCenterDelegate, CLLocationManagerDelegate> {
    Reachability *_internetReachability;
    CLLocationManager *locationManager;
}

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [[UINavigationBar appearance] setBarTintColor:BASEAPPEARANCECOLOUR];
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UITabBar appearance] setTintColor:BASEAPPEARANCECOLOUR];
    
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]];

    _internetReachability = [Reachability reachabilityForInternetConnection];
    [_internetReachability startNotifier];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    [self iQKeboardManager];
    [self registerForRemoteNotifications];
    [self setUpLocationManager];
    
    [self createAPPSUPPORTDIRECTORY];
    [self createDatabase];

    return YES;
}

- (void)createAPPSUPPORTDIRECTORY {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:APPSUPPORTDIRECTORY isDirectory:NULL]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtPath:APPSUPPORTDIRECTORY withIntermediateDirectories:YES attributes:nil error:&error]) {
            NSURL *url = [NSURL fileURLWithPath:APPSUPPORTDIRECTORY];
            if (![url setResourceValue:@YES forKey:NSURLIsExcludedFromBackupKey error:&error]) {
                NSLog(@"Error excluding %@ from backup %@", url.lastPathComponent, error.localizedDescription);
            }
            else {
                NSLog(@"Yay NSURLIsExcludedFromBackupKey");
            }
        }
        else {
            NSLog(@"%@", error.localizedDescription);
        }
    }
}

- (void)createDatabase {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *writableDBPath = BUSKETPLISTPATH;
    if ([fileManager fileExistsAtPath:writableDBPath]) {
        NSLog(@"plist created");
    }
    else {
        NSError *error = nil;
        NSString *defaultDBPath = [[NSBundle mainBundle] pathForResource:@"Busket" ofType:@"plist"];
        if ([fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error]) {
            NSLog(@"plist will create");
        }
        else {
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        }
    }
}

- (void)setUpLocationManager {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    //#warning Uncomment This
    if (SYSTEM_VERSION >= 8.0) {
        [locationManager requestWhenInUseAuthorization];
    }
    
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
}
    
- (void)registerForRemoteNotifications {
    if (SYSTEM_VERSION >= 10.0) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]){
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
//        else {
//            [[UIApplication sharedApplication]registerForRemoteNotificationTypes:
//             (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
//        }
    }
}
- (void)toggleSideMenu:(id)sender {
    [_container toggleLeftSideMenuCompletion:nil];
}

- (void)goForLogin {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    [_window.rootViewController presentViewController:[sb instantiateInitialViewController] animated:YES completion:nil];
}



- (void)checkLocationServicesAndStartUpdatesForController:(UIViewController*)controller {
    NSLog(@"[CLLocationManager authorizationStatus]===%d",[CLLocationManager authorizationStatus]);
    if (![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus] <= kCLAuthorizationStatusDenied) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location Services Disabled!" message:@"Please enable Location Based Services for better results! We promise to keep your location private" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (![CLLocationManager locationServicesEnabled]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=LOCATION_SERVICES"]];
            }
            else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:nil]];
        [controller presentViewController:alert animated:YES completion:nil];
        
    }
    else {
        //Location Services Enabled, let's start location updates
        [locationManager startUpdatingLocation];
    }
}
    

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError: %@", error);
    [locationManager stopUpdatingLocation];
}
    
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"didUpdateToLocation: %@", newLocation);
    if (newLocation != nil) {
        [USERDEFAULTS setValue:[NSNumber numberWithDouble:newLocation.coordinate.latitude] forKey:@"LATITUDE"];
        [USERDEFAULTS setValue:[NSNumber numberWithDouble:newLocation.coordinate.longitude] forKey:@"LONGITUDE"];
        [USERDEFAULTS synchronize];
    }
    [locationManager stopUpdatingLocation];
}
 
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    NSString *deviceTokenString = [[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""]stringByReplacingOccurrencesOfString: @">" withString: @""]   stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSLog(@"the generated device token string is : %@",deviceTokenString);
    [User sharedUser].deviceToken = deviceTokenString;
}
    
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
    NSLog(@"Failed to get token, error: %@", error);
    [User sharedUser].deviceToken = @"";
}

#pragma mark - UNUserNotificationCenterDelegate
    //Called when a notification is delivered to a foreground app.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}
    
    //Called to let your app know which action was selected by the user for a given notification.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    completionHandler();
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [locationManager startUpdatingLocation];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) reachabilityChanged:(NSNotification *)note {
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    if ([curReach currentReachabilityStatus] == NotReachable) {
        [TSMessage showNotificationWithTitle:@"Network Error!" subtitle:@"Couldn't connect to the server. Please check your internet connection." type:TSMessageNotificationTypeError];
    }
}

- (void)iQKeboardManager {
    
    //ONE LINE OF CODE.
    //Enabling keyboard manager(Use this line to enable managing distance between keyboard & textField/textView).
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    //(Optional)Set Distance between keyboard & textField, Default is 10.
    //[[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15];
    
    //(Optional)Enable autoToolbar behaviour. If It is set to NO. You have to manually create UIToolbar for keyboard. Default is NO.
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    
    //(Optional)Setting toolbar behaviour to IQAutoToolbarBySubviews to manage previous/next according to UITextField's hierarchy in it's SuperView. Set it to IQAutoToolbarByTag to manage previous/next according to UITextField's tag property in increasing order. Default is `IQAutoToolbarBySubviews`.
    //[[IQKeyboardManager sharedManager] setToolbarManageBehaviour:IQAutoToolbarBySubviews];
    
    //(Optional)Resign textField if touched outside of UITextField/UITextView. Default is NO.
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    //(Optional)Giving permission to modify TextView's frame. Default is NO.
    //    [[IQKeyboardManager sharedManager] setCanAdjustTextView:YES];
    
    //(Optional)Show TextField placeholder texts on autoToolbar. Default is NO.
    [[IQKeyboardManager sharedManager] setShouldShowTextFieldPlaceholder:YES];
    
}
    
@end
