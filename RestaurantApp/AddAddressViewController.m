//
//  AddAddressViewController.m
//  RestaurantApp
//
//  Created by TM iMac on 1/11/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "AddAddressViewController.h"

@interface AddAddressViewController () {
    __weak IBOutlet UITextField *titleField;
    __weak IBOutlet UITextField *line1Field;
    __weak IBOutlet UITextField *line2Field;
    
    __weak IBOutlet UIButton *saveButton;
    __weak IBOutlet UITextField *countryField;
    __weak IBOutlet UITextField *postCodeField;
    __weak IBOutlet UITextField *cityField;
}

@end

@implementation AddAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (_address) {
        self.navigationItem.title = @"Update Address";
        [saveButton setTitle:@"Update" forState:UIControlStateNormal];
        titleField.text = _address.addressLabel;
        line1Field.text = _address.addressLine1;
        line2Field.text = _address.addressLine2;
        cityField.text = _address.townCity;
        postCodeField.text = _address.postCode;
        countryField.text = _address.county;
    }
    else {
        self.navigationItem.title = @"Add Address";
        [saveButton setTitle:@"Save" forState:UIControlStateNormal];
//        titleField.text = @"Office";
//        line1Field.text = @"House 238, Road 17";
//        line2Field.text = @"Mohakhali DOHS";
//        cityField.text = @"Dhaka";
//        postCodeField.text = @"1206";
//        countryField.text = @"Bangladesh";

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)saveBtnAct:(UIButton*)sender {
    if (_address == nil) {
        _address = [[Address alloc] init];
    }
    _address.addressLabel = titleField.text;
    _address.addressLine1 = line1Field.text;
    _address.addressLine2 = line2Field.text;
    _address.townCity = cityField.text;
    _address.postCode = postCodeField.text;
    _address.county = countryField.text;
    
    NSDictionary *param = [_address dictionary];
//    NSLog(@"Param===%@",param);
    if ([[sender titleForState:UIControlStateNormal] isEqualToString:@"Update"]) {
        [[APIManager sharedManager] executePUTRequestWith:[NSString stringWithFormat:@"EditAddress.php?restaurant_id=%@&customer_id=%ld&&address_id=%ld",RestaurantId, (long)[User sharedUser].customerId, (long)_address.addressId] Parameters:param ForSuccess:^(id response) {
            [TSMessage showNotificationWithTitle:[response valueForKey:@"Message"] type:TSMessageNotificationTypeSuccess];
            if(self.didUpdateAddress) {
                self.didUpdateAddress(_address);
                GoBackPopViewControllerAnimated(YES);
            }
        } ForFail:^(NSString *error) {
            [TSMessage showNotificationWithTitle:error type:TSMessageNotificationTypeError];
        }];
    }
    else {
        [[APIManager sharedManager] executePOSTRequestWith:[NSString stringWithFormat:@"AddNewAddress.php?restaurant_id=%@&customer_id=%ld",RestaurantId, (long)[User sharedUser].customerId] Parameters:param ForSuccess:^(id response) {
            [TSMessage showNotificationWithTitle:@"Address has been added successfully" type:TSMessageNotificationTypeSuccess];
            _address.addressId = [[[response valueForKey:@"Message"] valueForKey:@"address_id"] integerValue];
            if(self.didAddNewAddress) {
                self.didAddNewAddress(_address);
                GoBackPopViewControllerAnimated(YES);
            }
        } ForFail:^(NSString *error) {
            [TSMessage showNotificationWithTitle:error type:TSMessageNotificationTypeError];
        }];
    }
}

@end
