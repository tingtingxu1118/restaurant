//
//  Environment.h
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 8/4/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Environment : NSObject

+ (NSString*)baseURLstring;

@end
