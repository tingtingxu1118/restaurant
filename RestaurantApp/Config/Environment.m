//
//  Environment.m
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 8/4/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import "Environment.h"

@implementation Environment

+ (NSString*)select:(NSString*)key {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Environments" ofType:@"plist"];
    NSDictionary *enviroments = [[NSDictionary dictionaryWithContentsOfFile:path] valueForKey:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"Config"]];
    NSString *value = [enviroments valueForKey:key];
    if (value) {
        return value;
    }
    else {
        [NSException raise:@"Config Failed!" format:@"Couldn't find %@.", key];
    }
    return nil;
}

+ (NSString*)baseURLstring {
    return [Environment select:@"baseServiceUrl"];
}

@end
