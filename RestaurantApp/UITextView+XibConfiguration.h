//
//  UITextView+XibConfiguration.h
//  HeadsUp7
//
//  Created by TM iMac on 1/30/17.
//  Copyright © 2017 tm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (XibConfiguration)

@property (nonatomic) IBInspectable NSString *placeholder;

- (void)updatePlaceholderVisibility;

@end
