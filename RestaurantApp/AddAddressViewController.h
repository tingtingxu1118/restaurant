//
//  AddAddressViewController.h
//  RestaurantApp
//
//  Created by TM iMac on 1/11/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIManager.h"

typedef void(^AddressBlock)(Address *address);

@interface AddAddressViewController : UIViewController

@property (nonatomic, retain) Address *address;

@property (copy) AddressBlock didAddNewAddress;
@property (copy) AddressBlock didUpdateAddress;

@end
