//
//  AppDelegate.h
//  RestaurantApp
//
//  Created by TM iMac on 11/2/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MFSideMenu/MFSideMenu.h>
#import "TabBarController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) MFSideMenuContainerViewController *container;

- (void)toggleSideMenu:(id)sender;
- (void)goForLogin;
- (void)checkLocationServicesAndStartUpdatesForController:(UIViewController*)controller;
    
@end

