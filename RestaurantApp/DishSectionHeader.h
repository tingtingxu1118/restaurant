//
//  DishSectionHeader.h
//  RestaurantApp
//
//  Created by TM iMac on 12/18/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@protocol DishSectionHeaderDelegate;
@interface DishSectionHeader : UITableViewHeaderFooterView {
    __weak IBOutlet UILabel *descriptionLbl;
    __weak IBOutlet UILabel *nameLbl;
    __weak IBOutlet UIButton *addBtn;
    __weak IBOutlet UILabel *priceLbl;
}

@property (nonatomic, retain) Dish *dish;
@property (nonatomic, weak) id<DishSectionHeaderDelegate> delegate;
@property (nonatomic, weak) IBOutlet UILabel *separetor;

- (CGFloat)layoutHeightForTableView:(UITableView *)tableView;

@end

@protocol DishSectionHeaderDelegate <NSObject>
@optional
- (void)goForAddItem:(id)sender;

@end
