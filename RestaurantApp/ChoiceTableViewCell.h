//
//  ChoiceTableViewCell.h
//  RestaurantApp
//
//  Created by TM iMac on 12/18/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@protocol ChoiceTableViewCellDelegate;

@interface ChoiceTableViewCell : UITableViewCell {
    __weak IBOutlet UILabel *titleLbl;
    __weak IBOutlet UILabel *priceLbl;
}

@property (nonatomic, retain) DishCombination *dishCombination;
@property (nonatomic, weak) id<ChoiceTableViewCellDelegate> delegate;
@property (nonatomic, retain) Dish *dish;

@end


@protocol ChoiceTableViewCellDelegate <NSObject>
@optional
- (void)goForAddItem:(ChoiceTableViewCell*)sender;

@end
