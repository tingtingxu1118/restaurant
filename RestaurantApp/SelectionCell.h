//
//  SelectionCell.h
//  RestaurantApp
//
//  Created by Shah Newaz Hossain on 2/1/17.
//  Copyright © 2017 DreamAppSolution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataModels.h"

@interface SelectionCell : UITableViewCell {
    __weak IBOutlet UILabel *nameLbl;
    __weak IBOutlet UIImageView *selectBtn;
}

@property (nonatomic, retain) GroupsChoice *groupChoice;
@property (nonatomic, retain) Choice *choice;

@end
