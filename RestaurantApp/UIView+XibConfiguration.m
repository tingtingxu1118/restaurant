//
//  UIView+XibConfiguration.m
//  RestaurantApp
//
//  Created by TM iMac on 11/29/16.
//  Copyright © 2016 DreamAppSolution. All rights reserved.
//

#import "UIView+XibConfiguration.h"

@implementation UIView (XibConfiguration)
@dynamic borderColor, borderWidth, cornerRadius;

- (void)setBorderColor:(UIColor *)borderColor{
    [self.layer setBorderColor:borderColor.CGColor];
}

- (void)setBorderWidth:(CGFloat)borderWidth{
    [self.layer setBorderWidth:borderWidth];
}

- (void)setCornerRadius:(CGFloat)cornerRadius{
    [self.layer setCornerRadius:cornerRadius];
}


@end
